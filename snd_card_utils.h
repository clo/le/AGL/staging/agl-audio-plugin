/***
snd_card_utils.h -- providing ucm support for agl-audio-plugin

Copyright (c) 2016, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/

#ifndef paaglsndcardutils
#define paaglsndcardutils

#include "userdata.h"

/* USE CASE MAP ("phone", "HiFi", ...) */
typedef struct {
	char *media_role;
	char *ucm_verb;
	agl_ucm_type ucm_type;
} agl_audio_usecasemap;

typedef struct agl_audio_usecasemap_node{
	agl_audio_usecasemap usecasemap;
	struct agl_audio_usecasemap_node *next;
} agl_audio_usecasemap_node;

 /* AUDIO SINK MAP ("MultiMedia1", "MultiMedia2", ...) */
typedef struct {
	const char *name;
	const char *device;
	char *ucm_verb;
	uint32_t num_channels;
	uint32_t sampling_rate;
	uint32_t fragment_size;
	uint32_t fragment_count;
	const char *format;
	int mmap;
} agl_audio_sink_prop, agl_audio_source_prop;

typedef struct agl_audio_sink_prop_node{
	agl_audio_sink_prop sink_prop;
	uint32_t sink_index;
	struct agl_audio_sink_prop_node *next;
} agl_audio_sink_prop_node;

typedef struct agl_audio_source_prop_node{
	agl_audio_source_prop source_prop;
	uint32_t source_index;
	struct agl_audio_source_prop_node *next;
} agl_audio_source_prop_node;

typedef struct {
	pa_hook_slot    *sink_input_put;
	pa_hook_slot    *source_output_put;
	pa_hook_slot	*sink_input_unlink_post;
	pa_hook_slot    *source_output_unlink_post;
	pa_hook_slot    *sink_input_move_finish;
	pa_hook_slot    *source_output_move_finish;
	pa_hook_slot    *sink_state_changed;
	pa_hook_slot    *source_state_changed;
	pa_hook_slot    *sink_input_volume_changed;
	pa_hook_slot    *sink_input_mute_changed;
} agl_snd_card_utils_hooks;

int agl_snd_card_utils_init (struct userdata *u);
int agl_snd_card_utils_done (struct userdata *u);
agl_ucm_type agl_ucm_type_from_str (const char *str);
int agl_snd_card_utils_add_usecasemap (const char *src_media_role, const char *src_ucm_verb);
int agl_snd_card_utils_add_sink_prop (agl_audio_sink_prop *args);
int agl_snd_card_utils_add_sink (struct userdata *u, agl_audio_sink_prop_node *sink_prop_node);
int agl_snd_card_utils_add_source_prop (agl_audio_source_prop *args);
int agl_snd_card_utils_add_source (struct userdata *u, agl_audio_source_prop_node *source_prop_node);
char *agl_snd_card_utils_get_ucm_verb_from_sink_name(const char *sink_name);
char *agl_snd_card_utils_get_ucm_verb_from_source_name(const char *source_name);
char *agl_snd_card_utils_get_media_role_from_sink_name(const char *sink_name);
char *agl_snd_card_utils_get_media_role_from_source_name(const char *source_name);
int agl_snd_card_utils_put_ucm_verb_by_sink_name(const char *sink_name, const char *real_verb);
int agl_snd_card_utils_put_ucm_verb_by_source_name(const char *source_name, const char *real_verb);
#endif
