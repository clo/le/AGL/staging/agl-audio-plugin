/***
snd_card_utils.c -- providing ucm support for agl-audio-plugin

Copyright (c) 2016, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/

#include "classify.h"
#include "node.h"
#include "router.h"
#include "snd_card_utils.h"
#include <stdio.h>
#include <math.h>
#include <pulsecore/core-util.h>
#include <pulsecore/pulsecore-config.h>	/* required for "module.h" */
#include <pulsecore/module.h>
#include <alsa/use-case.h> /* for UCM */
#include <alsa/asoundlib.h>

#define DEFAULT_SINK_USECASE "HiFi"
#define DEFAULT_SOURCE_USECASE "Record"
#define TOTAL_INIT_RETRIES 10
#define IDENTIFIER_LEN 32 /* Define enough buffer for identifier checking */

/*************************************************************************/
/*                  STATIC STRUCTURE DECLARATIONS                        */
/*************************************************************************/
static agl_audio_sink_prop_node *sink_prop_list_head = NULL;
static agl_audio_source_prop_node *source_prop_list_head = NULL;
static agl_audio_usecasemap_node *usecasemap_list_head = NULL;
static agl_snd_card_utils_hooks pa_hooks;
static char current_use_case_verb[128] = SND_USE_CASE_VERB_INACTIVE;
static bool current_use_case_verb_active = false;
static char hfp_volume_ctl_name[] = "PRI AUXPCM LOOPBACK Volume";

static char identifier_verb[IDENTIFIER_LEN] = "_verb";
static char identifier_enamods[IDENTIFIER_LEN] = "_enamods";

static int bt_init_volume = 8192;
/*************************************************************************/
/*                  STATIC FUNCTION DECLARATIONS                         */
/*************************************************************************/
static void agl_snd_card_utils_mixer_set_volume(int volume, const char *ctl_name);

static int agl_snd_utils_get_ucm_sound_card_name(char *snd_card_name)
{
	FILE *fp;
	int err = -PA_ERR_NOENTITY;
	char snd_card_info[200];
	char *snd_card_info_parse = NULL;

	if ((fp = fopen("/proc/asound/cards", "r")) == NULL ){
		pa_log_info("Cannot open /proc/asound/cards file to get sound card info");
		err = -PA_ERR_ACCESS;
		goto get_ucm_sound_card_name_end;
	}

	if ((fgets(snd_card_info, sizeof(snd_card_info), fp) != NULL)) {
		pa_log_info("Found cards %s", snd_card_info);

		snd_card_info_parse = strstr(snd_card_info, snd_card_name);
		if (snd_card_info_parse && !strncmp(snd_card_info_parse, snd_card_name, strlen(snd_card_name))){
			pa_log_info("Found matching card %s", snd_card_name);
			err = PA_OK;
			goto get_ucm_sound_card_name_end;
		}
		else{
			err = -PA_ERR_INVALID;
		}
	}

get_ucm_sound_card_name_end:
	if (fp)
		fclose(fp);

	return err;
}

static int agl_snd_utils_find_ucm_verb (const char *role, char **ret_ucm_verb)
{
	agl_audio_usecasemap_node *usecasemap_node = usecasemap_list_head;
	int err = -PA_ERR_INVALID;

	pa_assert (ret_ucm_verb);

	pa_log_info("Media Role from property is %s", role);

	while (usecasemap_node != NULL) {
		if (!strncmp(role, usecasemap_node->usecasemap.media_role, strlen(role)+1)){
			*ret_ucm_verb = usecasemap_node->usecasemap.ucm_verb;
			err = 0;
			break;
		}
		usecasemap_node = usecasemap_node->next;
	}

	return err;
}

/* Activate a ucm verb by set verb or modifier */
static int agl_snd_utils_activate_verb (struct userdata *u, const char *ucm_verb)
{
	int err = 0;

	/* If the current use-case verb is used but not active, the routing path should already */
	/* be active. The active boolean is used to help deinitialize routing when use-cases are */
	/* running concurrently. */
	if (!current_use_case_verb_active && (!strncmp(current_use_case_verb, ucm_verb, strlen(ucm_verb)+1))){
		current_use_case_verb_active = true;
	}
	else{
		/* If there are no use-cases running, simply enable a verb */
		if (!strncmp(current_use_case_verb, SND_USE_CASE_VERB_INACTIVE, strlen(SND_USE_CASE_VERB_INACTIVE)+1)){
			err = snd_use_case_set(u->ucm_mgr, identifier_verb, ucm_verb);
			if (err < 0){
				pa_log_info("UCM Verb %s failed to set and returned err=%d",
					ucm_verb, err);
				goto end;
			}
			strncpy(current_use_case_verb, ucm_verb, strlen(ucm_verb)+1);
			current_use_case_verb_active = true;
			pa_log_info("UCM Verb %s set", ucm_verb);
		}

		/* If the verb is already being used */
		else if (!strncmp(current_use_case_verb, ucm_verb, strlen(ucm_verb)+1)){
			pa_log_info("Current verb %s already active", ucm_verb);
		}

		/* If there is already a verb active and another use-case needs mixer controls */
		else{
			err = snd_use_case_set(u->ucm_mgr, "_enamod", ucm_verb);
			if (err < 0){
				pa_log_info("UCM Modifier %s failed to set and returned err=%d",
					ucm_verb, err);
				goto end;
			}
			pa_log_info("UCM Modifier %s set", ucm_verb);
		}
	}
end:
	return err;
}

/* Inactivate a ucm verb by set SND_USE_CASE_VERB_INACTIVE */
static int agl_snd_utils_inactivate_verb (struct userdata *u, const char *ucm_verb)
{
	const char **ucm_mod_list = NULL;
	int ucm_mod_list_size, err = 0;

	ucm_mod_list_size = snd_use_case_get_list(u->ucm_mgr,
		identifier_enamods, &ucm_mod_list);
	pa_log_info("UCM Modifier List size is %d", ucm_mod_list_size);
	err = snd_use_case_free_list(ucm_mod_list, ucm_mod_list_size);
	if (err < 0){
		pa_log_info("Failed to free UCM Modifer list");
		goto end;
	}

	/* If the current use-case verb is being disabled but a modifier is still running, */
	/* queue the verb disable on the next unlink callback */
	if ((ucm_mod_list_size > 0) &&
		!strncmp(current_use_case_verb, ucm_verb, strlen(ucm_verb)+1)){
		current_use_case_verb_active = false;
	}

	/* If there are modifiers running and the current use-case is not being disabled, */
	/* try and disable the modifier. */
	else if (ucm_mod_list_size > 0){
		err = snd_use_case_set(u->ucm_mgr, "_dismod", ucm_verb);
		if (err < 0){
			pa_log_info("UCM Modifier %s failed to dismod and returned err=%d",
				ucm_verb, err);
			goto end;
		}

		/* If this is the last modifier and current verb is not active, make current verb inactive */
		if ((ucm_mod_list_size == 1) && !current_use_case_verb_active){
			err = snd_use_case_set(u->ucm_mgr, identifier_verb, SND_USE_CASE_VERB_INACTIVE);
			if (err < 0){
				pa_log_info("UCM Verb %s failed to Inactive and returned err=%d",
					SND_USE_CASE_VERB_INACTIVE, err);
				goto end;
			}

			strncpy(current_use_case_verb, SND_USE_CASE_VERB_INACTIVE, strlen(SND_USE_CASE_VERB_INACTIVE)+1);
			pa_log_info("UCM Verb %s set to Inactive", ucm_verb);
		}
	}

	/* If there are no concurrent use-cases running */
	else{
		if (!strncmp(current_use_case_verb, ucm_verb, strlen(ucm_verb)+1)){
			err = snd_use_case_set(u->ucm_mgr, identifier_verb, SND_USE_CASE_VERB_INACTIVE);
			if (err < 0){
				pa_log_info("UCM Verb %s failed to Inactive and returned err=%d",
					SND_USE_CASE_VERB_INACTIVE, err);
				goto end;
			}
			strncpy(current_use_case_verb, SND_USE_CASE_VERB_INACTIVE, strlen(SND_USE_CASE_VERB_INACTIVE)+1);
			current_use_case_verb_active = false;
			pa_log_info("UCM Verb %s set to Inactive",
				ucm_verb);
		}
		else{
			pa_log_info("UCM Verb %s is not current active use-case %s", ucm_verb, current_use_case_verb);
			goto end;
		}
	}
end:
	return err;
}

static pa_hook_result_t agl_snd_card_utils_sink_input_put_cb (void *hook_data,
                                  void *call_data,
                                  void *slot_data)
{
	char *ucm_verb;
	const char *role;
	int err = 0;

	pa_log_info("Callback received for sink_input_put");
	pa_sink_input *data = (pa_sink_input *)call_data;
	struct userdata *u = (struct userdata *)slot_data;

	/* Using media role property to find UCM verb in usecasemap list */
	role = pa_proplist_gets (data->proplist, PA_PROP_MEDIA_ROLE);
	if (role == NULL) {
		ucm_verb = DEFAULT_SINK_USECASE;
	} else {
		err = agl_snd_utils_find_ucm_verb(role, &ucm_verb);
		if (err < 0) {
			pa_log_info("Invalid media role, use default sink verb.");
			ucm_verb = DEFAULT_SINK_USECASE;
		}
	}
	pa_assert(ucm_verb);

	/* Stream playback began, activate the ucm verb */
	err = agl_snd_utils_activate_verb(u, ucm_verb);
	if (err < 0)
		pa_log_error("%s error: ucm_verb=%s", __FUNCTION__, ucm_verb);

	return PA_HOOK_OK;
}

static pa_hook_result_t agl_snd_card_utils_source_output_put_cb (void *hook_data,
                                  void *call_data,
                                  void *slot_data)
{
	char *ucm_verb;
	const char *role;
	int err = 0;

	pa_log_info("Callback received for source_output_put");
	pa_source_output *data = (pa_source_output *)call_data;
	struct userdata *u = (struct userdata *)slot_data;

	/* Using media role property to find UCM verb in usecasemap list */
	role = pa_proplist_gets (data->proplist, PA_PROP_MEDIA_ROLE);
	if (role == NULL) {
		ucm_verb = DEFAULT_SOURCE_USECASE;
	} else {
		err = agl_snd_utils_find_ucm_verb(role, &ucm_verb);
		if (err < 0) {
			pa_log_info("Invalid media role, use default source verb.");
			ucm_verb = DEFAULT_SOURCE_USECASE;
		}
	}
	pa_assert(ucm_verb);

	/* Stream capture began, activate the ucm verb */
	err = agl_snd_utils_activate_verb(u, ucm_verb);
	if (err < 0)
		pa_log_error("%s error: ucm_verb=%s", __FUNCTION__, ucm_verb);

	if (!strncmp(ucm_verb, "Hfp_wb", strlen("Hfp_wb")) || !strncmp(ucm_verb, "Hfp", strlen("Hfp")))
		agl_snd_card_utils_mixer_set_volume(bt_init_volume, hfp_volume_ctl_name);

	return PA_HOOK_OK;
}

static pa_hook_result_t agl_snd_card_utils_sink_input_unlink_post_cb (void *hook_data,
                                  void *call_data,
                                  void *slot_data)
{
	const char *role;
	int err;
	struct alsa_userdata *alsau;

	pa_log_info("Callback received for sink_input_unlink_post");
	pa_sink_input *data = (pa_sink_input *)call_data;
	struct userdata *u = (struct userdata *)slot_data;

	role = pa_proplist_gets (data->proplist, PA_PROP_MEDIA_ROLE);
	if (!role) role = "none";

	if (pa_streq (role, "sdars_tuner_local_amp") 
		|| pa_streq (role, "hfp_uplink") || pa_streq (role, "hfp_downlink") 
		|| pa_streq (role, "hfp_wb_uplink") || pa_streq (role, "hfp_wb_downlink") 
		|| pa_streq (role, "icc_call")) {
		pa_sink *s = data->sink;
		pa_log_info ("Suspend sink immediately for hostless usecases!");
		pa_sink_suspend(s, true, PA_SUSPEND_IDLE );
	}

	return PA_HOOK_OK;
}

static pa_hook_result_t agl_snd_card_utils_source_output_unlink_post_cb (void *hook_data,
                                  void *call_data,
                                  void *slot_data)
{
	const char *role;
	int err;

	pa_log_info("Callback received for source_output_unlink_post");
	pa_source_output *data = (pa_source_output *)call_data;
	struct userdata *u = (struct userdata *)slot_data;

	role = pa_proplist_gets (data->proplist, PA_PROP_MEDIA_ROLE);
	if (!role) role = "none";

	if (pa_streq (role, "sdars_tuner_local_amp") 
		|| pa_streq (role, "hfp_uplink") || pa_streq (role, "hfp_downlink") 
		|| pa_streq (role, "hfp_wb_uplink") || pa_streq (role, "hfp_wb_downlink") 
		|| pa_streq (role, "icc_call")) {
		pa_source *s = data->source;
		pa_log_info ("Suspend source immediately for hostless usecases!");
		pa_source_suspend(s, true, PA_SUSPEND_IDLE );
	}

	return PA_HOOK_OK;
}

static pa_hook_result_t agl_snd_card_utils_sink_input_move_finish_cb (void *hook_data,
				void *call_data,
				void *slot_data)
{
	char *ucm_verb;
	const char *role;
	int err = 0;

	pa_sink_input *data = (pa_sink_input *)call_data;
	pa_sink *sink = data->sink;
	struct userdata *u = (struct userdata *)slot_data;

	/* Using media role property to find UCM verb in usecasemap list */
	role = pa_proplist_gets (data->proplist, PA_PROP_MEDIA_ROLE);
	if (role == NULL) {
		ucm_verb = DEFAULT_SINK_USECASE;
	} else {
		err = agl_snd_utils_find_ucm_verb(role, &ucm_verb);
		if (err < 0) {
			pa_log_info("Invalid media role, use default sink verb.");
			ucm_verb = DEFAULT_SINK_USECASE;
		}
	}
	pa_assert(ucm_verb);

	/*update real active ucm_verb behind sinks*/
	err = agl_snd_card_utils_put_ucm_verb_by_sink_name(sink->name, ucm_verb);
	if (err < 0)
		pa_log_error("%s error: ucm_verb=%s sink %s", __FUNCTION__, ucm_verb, sink->name);

	return PA_HOOK_OK;
}

static pa_hook_result_t agl_snd_card_utils_source_output_move_finish_cb (void *hook_data,
				void *call_data,
				void *slot_data)
{
	char *ucm_verb;
	const char *role;
	int err = 0;

	pa_source_output *data = (pa_source_output *)call_data;
	pa_source *source = data->source;
	struct userdata *u = (struct userdata *)slot_data;

	/* Using media role property to find UCM verb in usecasemap list */
	role = pa_proplist_gets (data->proplist, PA_PROP_MEDIA_ROLE);
	if (role == NULL) {
		ucm_verb = DEFAULT_SOURCE_USECASE;
	} else {
		err = agl_snd_utils_find_ucm_verb(role, &ucm_verb);
		if (err < 0) {
			pa_log_info("Invalid media role, use default source verb.");
			ucm_verb = DEFAULT_SOURCE_USECASE;
		}
	}
	pa_assert(ucm_verb);

	/*update real active ucm_verb behind sources*/
	err = agl_snd_card_utils_put_ucm_verb_by_source_name(source->name, ucm_verb);
	if (err < 0)
		pa_log_error("%s error: ucm_verb=%s source %s", __FUNCTION__, ucm_verb, source->name);

	return PA_HOOK_OK;
}

static pa_hook_result_t agl_snd_card_utils_sink_state_changed_cb (void *hook_data,
				void *call_data,
				void *slot_data)
{
	const char *ucm_verb, *dev_class, *role;
	int err = 0;
	pa_core *core;
	pa_sink_input *sinp;
	uint32_t index;

	pa_log_info("Callback received for sink_state_changed");
	pa_sink *data = (pa_sink *)call_data;
	struct userdata *u = (struct userdata *)slot_data;
	core = u->core;

	if (data->state != PA_SINK_SUSPENDED)
		goto sink_state_changed_end;

	/* Bypass null sinks with device.class "abstract" */
	dev_class = pa_proplist_gets(data->proplist, PA_PROP_DEVICE_CLASS);
	if (!dev_class || !pa_streq(dev_class, "sound"))
		goto sink_state_changed_end;

	/* FIXME: Find media role from sink name, and
	 * add media role to sink proplist for codec control use */
	role = agl_snd_card_utils_get_media_role_from_sink_name(data->name);
	if (!role) {
		pa_log_error("%s Failed to get media role from sink %s", __FUNCTION__, data->name);
		goto sink_state_changed_end;
	}
	pa_proplist_sets(data->proplist, PA_PROP_MEDIA_ROLE, role);

	ucm_verb = agl_snd_card_utils_get_ucm_verb_from_sink_name(data->name);
	pa_assert(ucm_verb);

	/* The alsa sink is suspended, clean the ucm verb from the list */
	PA_IDXSET_FOREACH(sinp, core->sink_inputs, index) {
		if(sinp && sinp->sink->index == data->index){
			pa_log_info("Stream still connected on the sink, keep the ucm verb in the list.");
			goto sink_state_changed_end;
		}
	}

	pa_log_info("Sink is suspended and no stream connected, clean the ucm verb from the list.");
	err = agl_snd_utils_inactivate_verb(u, ucm_verb);
	if (err < 0)
		pa_log_error("%s error: ucm_verb=%s", __FUNCTION__, ucm_verb);
	else
		pa_log_info("%s ucm verb control for %s succeed!", __FUNCTION__, role);

sink_state_changed_end:
	return PA_HOOK_OK;
}

static pa_hook_result_t agl_snd_card_utils_source_state_changed_cb (void *hook_data,
				void *call_data,
				void *slot_data)
{
	const char *ucm_verb, *dev_class, *role;
	int err = 0;
	pa_core *core;
	pa_source_output *sout;
	uint32_t index;

	pa_log_info("Callback received for source_state_changed");
	pa_source *data = (pa_source *)call_data;
	struct userdata *u = (struct userdata *)slot_data;
	core = u->core;

	if (data->state != PA_SOURCE_SUSPENDED)
		goto source_state_changed_end;

	/* Bypass monitor sources with device.class "monitor" */
	dev_class = pa_proplist_gets(data->proplist, PA_PROP_DEVICE_CLASS);
	if (!dev_class || !pa_streq(dev_class, "sound"))
		goto source_state_changed_end;

	/* FIXME: Find media role from source name, and
	 * add media role to source proplist for codec control use */
	role = agl_snd_card_utils_get_media_role_from_source_name(data->name);
	if (!role) {
		pa_log_error("%s Failed to get media role from source %s", __FUNCTION__, data->name);
		goto source_state_changed_end;
	}
	pa_proplist_sets(data->proplist, PA_PROP_MEDIA_ROLE, role);

	ucm_verb = agl_snd_card_utils_get_ucm_verb_from_source_name(data->name);
	pa_assert(ucm_verb);

	/* The alsa source is suspended, clean the ucm verb from the list */
	PA_IDXSET_FOREACH(sout, core->source_outputs, index) {
		if(sout && sout->source->index == data->index){
			pa_log_info("Stream still connected on the source, keep the ucm verb in the list.");
			goto source_state_changed_end;
		}
	}

	pa_log_info("Source is suspended and no stream connected, clean the ucm verb from the list.");
	err = agl_snd_utils_inactivate_verb(u, ucm_verb);
	if (err < 0)
		pa_log_error("%s error: ucm_verb=%s", __FUNCTION__, ucm_verb);
	else
		pa_log_info("%s ucm verb control for %s succeed!", __FUNCTION__, role);

source_state_changed_end:
	return PA_HOOK_OK;
}

static void agl_snd_card_utils_mixer_set_volume(int volume, const char *ctl_name)
{
	int err,index=0;
	char card[100];
	static snd_ctl_t *handle = NULL;
	snd_ctl_elem_id_t *id;
	snd_ctl_elem_value_t *control;

	snd_ctl_elem_id_alloca(&id);
	snd_ctl_elem_value_alloca(&control);

	index = -1;
	if (snd_card_next(&index) < 0 || index < 0) {
		pa_log_error("no soundcards found...");
		goto exit;
	}
	snprintf(card, sizeof(card), "hw:%d", index);

	if ((err = snd_ctl_open(&handle, card, 0)) < 0) {
		pa_log_error("Control %s open error: %s", card, snd_strerror(err));
		goto exit;
	}

	snd_ctl_elem_id_set_interface(id, SND_CTL_ELEM_IFACE_MIXER);
	snd_ctl_elem_id_set_name(id, ctl_name);

	snd_ctl_elem_value_set_id(control, id);
	if ((err = snd_ctl_elem_read(handle, control)) < 0) {
		pa_log_error("Cannot read the given element from control");
		goto exit;
	}

	snd_ctl_elem_value_set_integer(control, 0, volume);
	if ((err = snd_ctl_elem_write(handle, control)) < 0) {
		pa_log_error("Cannot write the given element from control");
		goto exit;
	}

exit:
	if (handle)
		snd_ctl_close(handle);
}


static pa_hook_result_t agl_snd_card_utils_sink_input_mute_changed_cb(void *hook_data,
                                  void *call_data,
                                  void *slot_data)
{
	return PA_HOOK_OK;
}


static pa_hook_result_t agl_snd_card_utils_sink_input_volume_changed_cb(void *hook_data,
                                  void *call_data,
                                  void *slot_data)
{
	pa_sink_input *sinp = (pa_sink_input *)call_data;
	struct userdata *u = (struct userdata *)slot_data;
	const char *role;
	char *ucm_verb;
	int32_t err, ret = 0;
	float value;
	int vol;

	role = pa_proplist_gets (sinp->proplist, PA_PROP_MEDIA_ROLE);
	if (role == NULL)
		goto sink_input_volume_end;

	err = agl_snd_utils_find_ucm_verb(role, &ucm_verb);
	if (err < 0)
	{
		pa_log_info("Invalid media role.");
		goto sink_input_volume_end;
	}

	if (!strncmp(ucm_verb, SND_USE_CASE_VERB_INACTIVE, strlen(SND_USE_CASE_VERB_INACTIVE)+1))
		goto sink_input_volume_end;

	if ((!strcmp(ucm_verb, "Hfp_wb")) || (!(strcmp(ucm_verb, "Hfp")))) {
		value = (float)sinp->volume.values[0];
		if (value < 0.0) {
			pa_log_info("%s: (%f) Under 0.0, assuming 0.0\n", __func__, value);
			value = 0.0f;
		} else {
			value = ((value > 15.000000f) ? 1.0f : (value / 15));
			pa_log_info("%s: Volume brought with in range (%f)\n", __func__, value);
		}
		bt_init_volume = vol = (int)rintf((value * 0x2000) + 0.5f);

		agl_snd_card_utils_mixer_set_volume(vol, hfp_volume_ctl_name);
	} else
		goto sink_input_volume_end;

sink_input_volume_end:
	return PA_HOOK_OK;
}



/*************************************************************************/
/*                  FUNCTION DECLARATIONS                                */
/*************************************************************************/

int agl_snd_card_utils_init (struct userdata *u)
{
	int num_verbs, retry_cnt, i, err = 0;
	bool snd_card_found = false;
	char card_name[256] = "sa8155-adp-star-snd-card";
	const char **verb_list;
	agl_audio_sink_prop_node *sink_prop_node;
	agl_audio_source_prop_node *source_prop_node;
	pa_hook *hooks;

	pa_assert (u);
	pa_assert_se (hooks = u->core->hooks);

	/* sink_input_put priority = PA_HOOK_EARLY + 1, later than module-acdb PA_HOOK_EARLY
	 * to ensure acdb paramter effective.
	 * sink_input_move_finish and source_output_move_finish priority = PA_HOOK_EARLY + 3, a little
	 * bit earlier than pulseaudio-module-codec-control and suspend-on-idle, to ensure sequence of
	 * codec, ADSP and ALSA devices. */
	pa_hooks.sink_input_put = pa_hook_connect (hooks + PA_CORE_HOOK_SINK_INPUT_PUT,
		                             (PA_HOOK_EARLY + 1), agl_snd_card_utils_sink_input_put_cb, u);
	pa_hooks.source_output_put = pa_hook_connect (hooks + PA_CORE_HOOK_SOURCE_OUTPUT_PUT,
		                               ( PA_HOOK_EARLY + 1), agl_snd_card_utils_source_output_put_cb, u);
	pa_hooks.sink_input_unlink_post = pa_hook_connect (hooks + PA_CORE_HOOK_SINK_INPUT_UNLINK_POST,
						(PA_HOOK_LATE + 1), agl_snd_card_utils_sink_input_unlink_post_cb, u);
	pa_hooks.source_output_unlink_post = pa_hook_connect (hooks + PA_CORE_HOOK_SOURCE_OUTPUT_UNLINK_POST,
						(PA_HOOK_LATE + 1), agl_snd_card_utils_source_output_unlink_post_cb, u);
	pa_hooks.sink_input_move_finish = pa_hook_connect (hooks + PA_CORE_HOOK_SINK_INPUT_MOVE_FINISH,
						(PA_HOOK_EARLY + 3), agl_snd_card_utils_sink_input_move_finish_cb, u);
	pa_hooks.source_output_move_finish = pa_hook_connect (hooks + PA_CORE_HOOK_SOURCE_OUTPUT_MOVE_FINISH,
						(PA_HOOK_EARLY + 3), agl_snd_card_utils_source_output_move_finish_cb, u);
	pa_hooks.sink_state_changed = pa_hook_connect (hooks + PA_CORE_HOOK_SINK_STATE_CHANGED,
						(PA_HOOK_LATE + 1), agl_snd_card_utils_sink_state_changed_cb, u);
	pa_hooks.source_state_changed = pa_hook_connect (hooks + PA_CORE_HOOK_SOURCE_STATE_CHANGED,
						(PA_HOOK_LATE + 1), agl_snd_card_utils_source_state_changed_cb, u);
	pa_hooks.sink_input_volume_changed = pa_hook_connect (hooks + PA_CORE_HOOK_SINK_INPUT_VOLUME_CHANGED,
		                                (PA_HOOK_LATE + 1),agl_snd_card_utils_sink_input_volume_changed_cb, u);
	pa_hooks.sink_input_mute_changed = pa_hook_connect (hooks + PA_CORE_HOOK_SINK_INPUT_MUTE_CHANGED,
		                                (PA_HOOK_LATE + 1), agl_snd_card_utils_sink_input_mute_changed_cb, u);

	/* Try and find the proper sound card.  Retry five times before failing */
	retry_cnt = 0;
	while(!snd_card_found && (retry_cnt < TOTAL_INIT_RETRIES)){

		err = agl_snd_utils_get_ucm_sound_card_name(card_name);
		if (err == 0){
			snd_card_found = true;
			break;
		}

		if (!snd_card_found){
			retry_cnt++;
			pa_log_info("agl_snd_utils_get_ucm_sound_card_name returned with err %d", err);
			if (retry_cnt >= TOTAL_INIT_RETRIES){
				pa_log_info("Could not find sound card %s", card_name);
				goto end;
			}

			pa_log_info("%s card not found. Sleeping for 1s", card_name);
			usleep(1000000);
		}
	}

	err = snd_use_case_mgr_open(&u->ucm_mgr, card_name);
	if (err < 0){
		pa_log_info("UCM not available for card %s", card_name);
		goto end;
	}

	pa_log_info("UCM available for card %s", card_name);

	/* get a list of all UCM verbs (profiles) for this card */
	num_verbs = snd_use_case_verb_list(u->ucm_mgr, &verb_list);
	if (num_verbs < 0){
		pa_log("UCM verb list not found for %s", card_name);
	}

	for (i = 0; i < num_verbs; i++){
		pa_log_info("UCM Verb %s found", verb_list[i]);
	}

	/* Create audio sinks from properties obtained from JSON config*/
	sink_prop_node = sink_prop_list_head;
	while (sink_prop_node != NULL)
	{
		agl_snd_card_utils_add_sink(u, sink_prop_node);
		sink_prop_node = sink_prop_node->next;
	}

	/* Create audio sources from properties obtained from JSON config*/
	source_prop_node = source_prop_list_head;
	while (source_prop_node != NULL)
	{
		agl_snd_card_utils_add_source(u, source_prop_node);
		source_prop_node = source_prop_node->next;
	}

	/* Activate verbs again before sink/source suspend */
	const char *verb = "HiFi";
	err = agl_snd_utils_activate_verb(u, verb);
	verb = "Navigation";
	err |= agl_snd_utils_activate_verb(u, verb);
	verb = "SDARS Tuner Local AMP";
	err |= agl_snd_utils_activate_verb(u, verb);
	verb = "Record";
	err |= agl_snd_utils_activate_verb(u, verb);
	verb = "Icc_call";
	err |= agl_snd_utils_activate_verb(u, verb);
	if (err)
		pa_log_error("%s Init verb fail with err = %d", __FUNCTION__, err);

end:
	return err;
}

int agl_snd_card_utils_done (struct userdata *u){
	agl_audio_sink_prop_node *sink_prop_node;
	agl_audio_source_prop_node *source_prop_node;
	agl_audio_usecasemap_node *usecasemap_node;
	pa_core      *core;
	pa_sink      *sink;
	pa_source    *source;
	int err = 0;

	pa_log_info ("Closing agl_snd_card_utils");

	/* Unload sinks */
	sink_prop_node = sink_prop_list_head;
	while (sink_prop_node != NULL)
	{
		if (u && (core = u->core)) {
			if ((sink = pa_idxset_get_by_index (core->sinks, sink_prop_node->sink_index))){
				if (sink->module){
					pa_log_info ("unloading alsa sink '%s'", sink_prop_node->sink_prop.name);
					pa_module_unload (sink->module, false);
				}
				else{
					pa_log_info ("Unable to find module for sink '%s'", sink_prop_node->sink_prop.name);
				}
			}
		}
		sink_prop_node = sink_prop_node->next;
		pa_xfree(sink_prop_list_head);
		sink_prop_list_head = sink_prop_node;
	}

	/* Unload sources */
	source_prop_node = source_prop_list_head;
	while (source_prop_node != NULL)
	{
		if (u && (core = u->core)) {
			if ((source = pa_idxset_get_by_index (core->sources, source_prop_node->source_index))){
				if (source->module){
					pa_log_info ("unloading alsa source '%s'", source_prop_node->source_prop.name);
					pa_module_unload (source->module, false);
				}
				else{
					pa_log_info ("Unable to find module for source '%s'", source_prop_node->source_prop.name);
				}
			}
		}
		source_prop_node = source_prop_node->next;
		pa_xfree(source_prop_list_head);
		source_prop_list_head = source_prop_node;
	}

	/* Free usecasemap */
	usecasemap_node = usecasemap_list_head;
	while (usecasemap_node != NULL)
	{
		usecasemap_node = usecasemap_node->next;
		pa_xfree(usecasemap_list_head->usecasemap.media_role);
		pa_xfree(usecasemap_list_head->usecasemap.ucm_verb);
		pa_xfree(source_prop_list_head);
		usecasemap_list_head = usecasemap_node;
	}

	if(u && u->ucm_mgr) {
		err = snd_use_case_mgr_close(u->ucm_mgr);
		if (err < 0){
			pa_log_info("UCM failed to close");
		}
		else{
			pa_log_info("UCM closed");
			u->ucm_mgr = NULL;
		}
	}

	return err;
}

agl_ucm_type agl_ucm_type_from_str (const char *str)
{
	agl_ucm_type type;

	pa_assert (str);
	if (pa_streq (str, "HiFi"))
		type = agl_ucm_hifi;
	else if (pa_streq (str, "Record"))
		type = agl_ucm_record;
	else if (pa_streq (str, "Navigation"))
		type = agl_ucm_navigation;
	else if (pa_streq (str, "Hfp"))
		type = agl_ucm_hfp;
	else if (pa_streq (str, "Hfp_wb"))
		type = agl_ucm_hfp_wb;
	else if (pa_streq (str, "SDARS Tuner Local AMP"))
		type = agl_ucm_sdars_tuner_local_amp;
	else if (pa_streq (str, "Voice_ECNS"))
		type = agl_ucm_voice_ecns;
	else if (pa_streq (str, "Icc_call"))
		type = agl_ucm_icc_call;
	else
		type = agl_ucm_type_unknown;

	return type;
}

int agl_snd_card_utils_add_usecasemap (const char *src_media_role, const char *src_ucm_verb) {
	agl_audio_usecasemap_node *usecasemap_node = usecasemap_list_head;

	if (usecasemap_node != NULL)
	{
		while (usecasemap_node->next != NULL)
		{
			usecasemap_node = usecasemap_node->next;
		}

		usecasemap_node->next = pa_xnew0 (agl_audio_usecasemap_node, 1);
		usecasemap_node = usecasemap_node->next;
	}
	else
	{
		usecasemap_node = pa_xnew0 (agl_audio_usecasemap_node, 1);
		usecasemap_list_head = usecasemap_node;
	}

	usecasemap_node->usecasemap.media_role = pa_xstrdup (src_media_role);
	usecasemap_node->usecasemap.ucm_verb = pa_xstrdup (src_ucm_verb);
	usecasemap_node->usecasemap.ucm_type = agl_ucm_type_from_str (src_ucm_verb);
	usecasemap_node->next = NULL;

pa_log_info("Added usecasemap with media role %s and UCM verb %s, ucm type is %d\n",
		usecasemap_node->usecasemap.media_role, usecasemap_node->usecasemap.ucm_verb, usecasemap_node->usecasemap.ucm_type);

	return 0;
}

int agl_snd_card_utils_add_sink_prop (agl_audio_sink_prop *args) {
	agl_audio_sink_prop_node *sink_node = sink_prop_list_head;

	if (sink_node != NULL)
	{
		while (sink_node->next != NULL)
		{
			sink_node = sink_node->next;
		}
		
		sink_node->next = pa_xnew0 (agl_audio_sink_prop_node, 1);
		sink_node = sink_node->next;
	}
	else
	{
		sink_node = pa_xnew0 (agl_audio_sink_prop_node, 1);
		sink_prop_list_head = sink_node;
	}

	memcpy(sink_node, args, sizeof(agl_audio_sink_prop));
	sink_node->sink_index = 0;
	sink_node->next = NULL;

	pa_log_info("Added sink with name %s\n", sink_node->sink_prop.name);
	pa_log_info("Device %s\n", sink_node->sink_prop.device);
	pa_log_info("UCM Verb %s\n", sink_node->sink_prop.ucm_verb);
	pa_log_info("Number of Channels = %d\n", sink_node->sink_prop.num_channels);
	pa_log_info("Sampling Rate = %d\n", sink_node->sink_prop.sampling_rate);
	pa_log_info("Fragment Size = %d\n", sink_node->sink_prop.fragment_size);
	pa_log_info("Fragment Count = %d\n", sink_node->sink_prop.fragment_count);
	pa_log_info("Format %s\n", sink_node->sink_prop.format);
	pa_log_info("Mmap %d\n", sink_node->sink_prop.mmap);
	return 0;
}

int agl_snd_card_utils_add_source_prop (agl_audio_source_prop *args) {
	agl_audio_source_prop_node *source_node = source_prop_list_head;

	if (source_node != NULL)
	{
		while (source_node->next != NULL)
		{
			source_node = source_node->next;
		}
		
		source_node->next = pa_xnew0 (agl_audio_source_prop_node, 1);
		source_node = source_node->next;
	}
	else
	{
		source_node = pa_xnew0 (agl_audio_source_prop_node, 1);
		source_prop_list_head = source_node;
	}

	memcpy(source_node, args, sizeof(agl_audio_source_prop));
	source_node->source_index = 0;
	source_node->next = NULL;

	pa_log_info("Added source with name %s\n", source_node->source_prop.name);
	pa_log_info("Device %s\n", source_node->source_prop.device);
	pa_log_info("UCM Verb %s\n", source_node->source_prop.ucm_verb);
	pa_log_info("Number of Channels = %d\n", source_node->source_prop.num_channels);
	pa_log_info("Sampling Rate = %d\n", source_node->source_prop.sampling_rate);
	pa_log_info("Fragment Size = %d\n", source_node->source_prop.fragment_size);
	pa_log_info("Fragment Count = %d\n", source_node->source_prop.fragment_count);
	pa_log_info("Format %s\n", source_node->source_prop.format);
	pa_log_info("Mmap %d\n", source_node->source_prop.mmap);

	return 0;
}


int agl_snd_card_utils_add_sink (struct userdata *u, agl_audio_sink_prop_node *sink_prop_node)
{
	pa_core      *core;
	pa_module    *module;
	pa_sink      *sink;
	uint32_t      idx;
	char args[512];
	const char *ucm_verb = sink_prop_node->sink_prop.ucm_verb;
	int err = 0;

	pa_assert (u);
	pa_assert_se (core = u->core);

	snprintf (args, sizeof(args), "sink_name=%s " \
		"device=%s " \
		"channels=%d " \
		"rate=%d " \
		"fragment_size=%d " \
		"fragments=%d " \
		"format=%s " \
		"tsched=0 " \
		"mmap=%d",
		sink_prop_node->sink_prop.name, sink_prop_node->sink_prop.device,
		sink_prop_node->sink_prop.num_channels, sink_prop_node->sink_prop.sampling_rate,
		sink_prop_node->sink_prop.fragment_size, sink_prop_node->sink_prop.fragment_count,
		sink_prop_node->sink_prop.format, sink_prop_node->sink_prop.mmap);

	err = snd_use_case_set(u->ucm_mgr, identifier_verb, sink_prop_node->sink_prop.ucm_verb);
	if (err < 0){
		pa_log_info("UCM Verb %s failed to set and returned err=%d",
			sink_prop_node->sink_prop.ucm_verb, err);
		goto end;
	}

	pa_module_load(&module, core, "module-alsa-sink", args);

	if (!module){
		pa_log ("failed to load alsa sink '%s'", sink_prop_node->sink_prop.name);
		goto end;
	} else {
		PA_IDXSET_FOREACH(sink, core->sinks, idx){
			if (sink->module && sink->module == module){
				pa_log_info("created agl sink named '%s'", sink_prop_node->sink_prop.name);
				sink_prop_node->sink_index = sink->index;
				break;
			}
		}
	}

	err = snd_use_case_set(u->ucm_mgr, identifier_verb, SND_USE_CASE_VERB_INACTIVE);
	if (err < 0){
		pa_log_info("UCM Verb %s failed to set and returned err=%d",
			SND_USE_CASE_VERB_INACTIVE, err);
		goto end;
	}

end:
	return err;
}

int agl_snd_card_utils_add_source (struct userdata *u, agl_audio_source_prop_node *source_prop_node)
{
	pa_core      *core;
	pa_module    *module;
	pa_source      *source;
	uint32_t      idx;
	char args[512];
	const char *ucm_verb = source_prop_node->source_prop.ucm_verb;
	int err = 0;

	pa_assert (u);
	pa_assert_se (core = u->core);

	snprintf (args, sizeof(args), "source_name=%s " \
		"device=%s " \
		"channels=%d " \
		"rate=%d " \
		"fragment_size=%d " \
		"fragments=%d " \
		"format=%s " \
		"tsched=0 " \
		"mmap=%d",
		source_prop_node->source_prop.name, source_prop_node->source_prop.device,
		source_prop_node->source_prop.num_channels, source_prop_node->source_prop.sampling_rate,
		source_prop_node->source_prop.fragment_size, source_prop_node->source_prop.fragment_count,
		source_prop_node->source_prop.format, source_prop_node->source_prop.mmap);

	err = snd_use_case_set(u->ucm_mgr, identifier_verb, source_prop_node->source_prop.ucm_verb);
	if (err < 0){
		pa_log_info("UCM Verb %s failed to set and returned err=%d",
			source_prop_node->source_prop.ucm_verb, err);
		goto end;
	}

	pa_module_load(&module, core, "module-alsa-source", args);

	if (!module){
		pa_log ("failed to load alsa source '%s'", source_prop_node->source_prop.name);
		goto end;
	} else {
		PA_IDXSET_FOREACH(source, core->sources, idx){
			if (source->module && source->module == module){
				pa_log_info("created agl source named '%s'", source_prop_node->source_prop.name);
				source_prop_node->source_index = source->index;
				break;
			}
		}
	}

	err = snd_use_case_set(u->ucm_mgr, identifier_verb, SND_USE_CASE_VERB_INACTIVE);
	if (err < 0){
		pa_log_info("UCM Verb %s failed to set and returned err=%d",
			SND_USE_CASE_VERB_INACTIVE, err);
		goto end;
	}

end:
	return err;
}


char *agl_snd_card_utils_get_ucm_verb_from_sink_name(const char *sink_name){
	agl_audio_sink_prop_node *sink_prop_node = sink_prop_list_head;
	char *ret_name = NULL;

	while (sink_prop_node != NULL){
		if (!strncmp(sink_name, sink_prop_node->sink_prop.name, strlen(sink_name)+1)){
			ret_name = sink_prop_node->sink_prop.ucm_verb;
			break;
		}
		sink_prop_node = sink_prop_node->next;
	}

	return ret_name;
}

char *agl_snd_card_utils_get_ucm_verb_from_source_name(const char *source_name){
	agl_audio_source_prop_node *source_prop_node = source_prop_list_head;
	char *ret_name = NULL;

	while (source_prop_node != NULL){
		if (!strncmp(source_name, source_prop_node->source_prop.name, strlen(source_name)+1)){
			ret_name = source_prop_node->source_prop.ucm_verb;
			break;
		}
		source_prop_node = source_prop_node->next;
	}

	return ret_name;
}

char *agl_snd_card_utils_get_media_role_from_sink_name(const char *sink_name)
{
	agl_audio_usecasemap_node *usecasemap_node = usecasemap_list_head;
	char *ret_role = NULL;
	char *ucm_verb = agl_snd_card_utils_get_ucm_verb_from_sink_name(sink_name);

	if (!ucm_verb)
		return ret_role;

	while (usecasemap_node != NULL) {
		if (!strncmp(ucm_verb, usecasemap_node->usecasemap.ucm_verb, strlen(ucm_verb)+1)) {
			ret_role = usecasemap_node->usecasemap.media_role;
			break;
		}
		usecasemap_node = usecasemap_node->next;
	}

	return ret_role;
}

char *agl_snd_card_utils_get_media_role_from_source_name(const char *source_name)
{
	agl_audio_usecasemap_node *usecasemap_node = usecasemap_list_head;
	char *ret_role = NULL;
	char *ucm_verb = agl_snd_card_utils_get_ucm_verb_from_source_name(source_name);

	if (!ucm_verb)
		return ret_role;

	while (usecasemap_node != NULL) {
		if (!strncmp(ucm_verb, usecasemap_node->usecasemap.ucm_verb, strlen(ucm_verb)+1)) {
			ret_role = usecasemap_node->usecasemap.media_role;
			break;
		}
		usecasemap_node = usecasemap_node->next;
	}

	return ret_role;
}

int agl_snd_card_utils_put_ucm_verb_by_sink_name(const char *sink_name, const char *real_verb){
	int  err = -1;
	agl_audio_sink_prop_node *sink_prop_node = sink_prop_list_head;

	pa_log_info("%s sink name %s verb %s", __FUNCTION__, sink_name, real_verb);

	while (sink_prop_node != NULL){
		if (!strncmp(sink_name, sink_prop_node->sink_prop.name, strlen(sink_name)+1)){
			strncpy(sink_prop_node->sink_prop.ucm_verb, real_verb, strlen(real_verb)+1);
			err = 0;
			break;
		}
		sink_prop_node = sink_prop_node->next;
	}

	return err;
}

int agl_snd_card_utils_put_ucm_verb_by_source_name(const char *source_name, const char *real_verb){
	int err = -1;
	agl_audio_source_prop_node *source_prop_node = source_prop_list_head;

	pa_log_info("%s source name %s verb %s", __FUNCTION__, source_name, real_verb);

	while (source_prop_node != NULL){
		if (!strncmp(source_name, source_prop_node->source_prop.name, strlen(source_name)+1)){
			strncpy(source_prop_node->source_prop.ucm_verb, real_verb, strlen(real_verb)+1);
			err = 0;
			break;
		}
		source_prop_node = source_prop_node->next;
	}
	return err;
}
