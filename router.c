/*
 * module-agl-audio -- PulseAudio module for providing audio routing support
 * (forked from "module-murphy-ivi" - https://github.com/otcshare )
 * Copyright (c) 2012, Intel Corporation.
 * Copyright (c) 2016, IoT.bzh
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St - Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 */
#include <pulsecore/pulsecore-config.h> /* required for headers below */
#include <pulsecore/core-util.h>        /* required for "pa_streq" */
#include "router.h"
#include "switch.h"
#include "zone.h"
#include "utils.h"
#include "snd_card_utils.h"

agl_router *agl_router_init (struct userdata *u)
{
	agl_router *router;
	size_t num_classes;
	int i;

	num_classes = agl_application_class_end;

	router = pa_xnew0 (agl_router, 1);
	router->rtgroups.input = pa_hashmap_new (pa_idxset_string_hash_func,
		                                 pa_idxset_string_compare_func);
	router->rtgroups.output = pa_hashmap_new (pa_idxset_string_hash_func,
		                                  pa_idxset_string_compare_func);
	for (i = 0;  i < AGL_UCM_MAX;  i++) {
		router->behaviormatrix.casemap[i] = pa_hashmap_new (pa_idxset_string_hash_func,
		                                 pa_idxset_string_compare_func);
	}
	router->behaviormatrix.behaviormap = pa_hashmap_new (pa_idxset_string_hash_func,
		                                 pa_idxset_string_compare_func);

	router->maplen = num_classes;
        router->priormap = pa_xnew0 (int, num_classes);

	AGL_DLIST_INIT (router->nodlist);
	AGL_DLIST_INIT (router->connlist);

	return router;
}

void agl_router_done (struct userdata *u)
{
	agl_router *router;
	agl_node *e,*n;
	agl_connection *conn, *c;
	agl_rtgroup *rtg;
	agl_rtgroup **map;
	int i;

	if (u && (router = u->router)) {
		AGL_DLIST_FOR_EACH_SAFE(agl_node, rtprilist, e,n, &router->nodlist)
			AGL_DLIST_UNLINK(agl_node, rtprilist, e);
		AGL_DLIST_FOR_EACH_SAFE(agl_connection, link, conn,c, &router->connlist) {
			AGL_DLIST_UNLINK(agl_connection, link, conn);
			pa_xfree (conn);
		}
		/*
		PA_HASHMAP_FOREACH(rtg, router->rtgroups.input, state) {
			rtgroup_destroy(u, rtg);
		}
		PA_HASHMAP_FOREACH(rtg, router->rtgroups.output, state) {
			rtgroup_destroy(u, rtg);
		}*/
		pa_hashmap_free (router->rtgroups.input);
		pa_hashmap_free (router->rtgroups.output);
		for (i = 0;  i < AGL_UCM_MAX;  i++) {
			pa_hashmap_free (router->behaviormatrix.casemap[i]);
		}
		pa_hashmap_free (router->behaviormatrix.behaviormap);

		for (i = 0;  i < AGL_ZONE_MAX;  i++) {
			if ((map = router->classmap.input[i]))
				pa_xfree(map);
			if ((map = router->classmap.output[i]))
				pa_xfree(map);
		}

		pa_xfree (router->priormap);
		pa_xfree (router);

		u->router = NULL;
	}
}

bool agl_router_default_accept (struct userdata *u, agl_rtgroup *rtg, agl_node *node)
{
	/* TODO */
	return true;
}

bool agl_router_phone_accept (struct userdata *u, agl_rtgroup *rtg, agl_node *node)
{
	/* TODO */
	return true;
}

int agl_router_default_effect (struct userdata *u, agl_rtgroup *rtg, agl_node *node, bool new)
{
	/* TODO */
	return 1;
}

int agl_router_phone_effect (struct userdata *u, agl_rtgroup *rtg, agl_node *node, bool new)
{
	pa_assert (u);
	pa_assert (node);

	if (new)
		agl_utils_volume_ramp (u, node->nullsink, false);
	else
		agl_utils_volume_ramp (u, node->nullsink, true);

	return 1;
}

int agl_router_duck_effect (struct userdata *u, agl_rtgroup *rtg, agl_node *node, bool new)
{
	pa_assert (u);
	pa_assert (node);

	if (new) {
		pa_log_debug("[%d:%d] Apply ConDuck effect...", node->index, node->paidx);
		agl_utils_volume_duck (u, node, true);
	} else {
		pa_log_debug("[%d:%d] Resume ConDuck effect...", node->index, node->paidx);
		agl_utils_volume_duck (u, node, false);
	}

	return 1;
}

int agl_router_mute_effect (struct userdata *u, agl_rtgroup *rtg, agl_node *node, bool new)
{
	pa_assert (u);
	pa_assert (node);

	if (new) {
		pa_log_debug("[%d:%d] Apply ConMute effect...", node->index, node->paidx);
		agl_utils_volume_mute(u, node, true);
	} else {
		pa_log_debug("[%d:%d] Resume ConMute effect...", node->index, node->paidx);
		agl_utils_volume_mute(u, node, false);
	}

	return 1;
}

int agl_router_cork_effect (struct userdata *u, agl_rtgroup *rtg, agl_node *node, bool new)
{
	pa_assert (u);
	pa_assert (node);

	if (new) {
		pa_log_debug("[%d:%d] Apply SwitchCork effect...", node->index, node->paidx);
		agl_utils_cork(u, node, true);
	} else {
		pa_log_debug("[%d:%d] Resume SwitchCork effect...", node->index, node->paidx);
		agl_utils_cork(u, node, false);
	}

	return 1;
}

int agl_router_switch_effect (struct userdata *u, agl_rtgroup *rtg, agl_node *node, bool new)
{
	agl_router *router;
	agl_rtgroup *nodertg;
	agl_node *rtgnode;

	pa_assert (u);
	pa_assert (node);
	pa_assert_se (router = u->router);

	if (node->direction == agl_input)
		nodertg = pa_hashmap_get (router->rtgroups.input, agl_node_type_str (node->type));
	else
		nodertg = pa_hashmap_get (router->rtgroups.output, agl_node_type_str (node->type));
	if (nodertg)
		rtgnode = nodertg->node;
	else
		return 0;

	if (new) {
		pa_log_debug("[%d:%d] Apply SwitchNull effect...", node->index, node->paidx);
		agl_utils_switch2null(u, node, rtgnode->paname, true);
	} else {
		pa_log_debug("[%d:%d] Resume SwitchNull effect...", node->index, node->paidx);
		agl_utils_switch2null(u, node, rtgnode->paname, false);
	}

	return 1;
}

int agl_router_mix_effect (struct userdata *u, agl_rtgroup *rtg, agl_node *node, bool new)
{
	pa_assert (u);
	pa_assert (node);

	if (new) {
		pa_log_debug("[%d:%d] Apply ConMix effect...", node->index, node->paidx);
	} else {
		pa_log_debug("[%d:%d] Resume ConMix effect...", node->index, node->paidx);
	}

	return 1;
}

int agl_router_switchoff_effect (struct userdata *u, agl_rtgroup *rtg, agl_node *node, bool new)
{
	pa_core *core;
	pa_sink *sink;
	pa_sink_input *sinp;
	pa_source_output *sout;
	uint32_t index;

	pa_assert (u);
	pa_assert (node);
	pa_assert_se (core = u->core);

	if(new) {
		pa_log_debug("[%d:%d] Apply SwitchOff effect...", node->index, node->paidx);
		if (node->direction == agl_input) {
			if (!(sinp = pa_idxset_get_by_index(core->sink_inputs, node->paidx))) {
				pa_log_debug("can't find sink input for '%s'", node->amname);
			}
			else {
				pa_log_debug("Send right-lost event message...");
				pa_sink_input_send_event(sinp, PA_STREAM_EVENT_RIGHT_LOST, NULL);
				agl_utils_destroy_null_sink(u, node->nullsink);
			}
		} else {
			if (!(sout = pa_idxset_get_by_index(core->source_outputs, node->paidx))) {
				pa_log_debug("can't find source output for '%s'", node->amname);
			}
			else {
				pa_log_debug("Send right-lost event message...");
				pa_source_output_send_event(sout, PA_STREAM_EVENT_RIGHT_LOST, NULL);
				agl_utils_destroy_null_source(u, node->nullsource);
			}
		}
	}
	return 1;
}

int agl_router_reject_effect (struct userdata *u, agl_rtgroup *rtg, agl_node *node, bool new)
{
	pa_core *core;
	pa_sink_input *sinp;
	pa_source_output *sout;
	pa_client *client;

	pa_assert (u);
	pa_assert (node);
	pa_assert_se (core = u->core);

	if(new) {
		pa_log_debug("[%d:%d] Apply Reject effect...", node->index, node->paidx);
		if (node->direction == agl_input) {
			if (!(sinp = pa_idxset_get_by_index(core->sink_inputs, node->paidx))) {
				pa_log_debug("can't find sink input for '%s'", node->amname);
				return 1;
			}
			else {
				client = sinp->client;
				agl_utils_destroy_null_sink(u, node->nullsink);
			}
		} else {
			if (!(sout = pa_idxset_get_by_index(core->source_outputs, node->paidx))) {
				pa_log_debug("can't find source output for '%s'", node->amname);
				return 1;
			}
			else {
				client = sout->client;
				agl_utils_destroy_null_source(u, node->nullsource);
			}
		}

		pa_client_send_event(client, PA_STREAM_EVENT_RIGHT_LOST, NULL);
	}
	return 1;
}

agl_rtgroup_effect_t agl_router_get_effect_fct (const char * effect)
{
	agl_rtgroup_effect_t effect_fct;
	if (!effect)
		effect_fct = agl_router_default_effect;
	else if (pa_streq (effect, "phone"))
		effect_fct = agl_router_phone_effect;
	else if (pa_streq (effect, "duck"))
		effect_fct = agl_router_duck_effect;
	else if (pa_streq (effect, "mute"))
		effect_fct = agl_router_mute_effect;
	else if (pa_streq (effect, "switch"))
		effect_fct = agl_router_switch_effect;
	else if (pa_streq (effect, "cork"))
		effect_fct = agl_router_cork_effect;
	else if (pa_streq (effect, "switchoff"))
		effect_fct = agl_router_switchoff_effect;
	else
		effect_fct = agl_router_default_effect;

	return effect_fct;
}

agl_rtgroup *agl_router_create_rtgroup (struct userdata *u, agl_direction type, const char *name, const char *node_desc, agl_acceptset *acceptset, agl_rtgroup_effect_t effect)
{
	agl_router *router;
	agl_rtgroup *rtg;
	agl_nodeset *nodeset;
	agl_node *node;
	pa_hashmap *table;

	pa_assert (u);
	pa_assert (type == agl_input || type == agl_output);
	pa_assert (name);
	pa_assert_se (router = u->router);
	pa_assert_se (nodeset = u->nodeset);

	if (type == agl_input)
		table = router->rtgroups.input;
	else
		table = router->rtgroups.output;
	pa_assert (table);

	rtg = pa_xnew0 (agl_rtgroup, 1);
	rtg->name = pa_xstrdup (name);
	rtg->acceptset = acceptset;
	rtg->effect = effect;
	AGL_DLIST_INIT(rtg->entries);
	/* associate an agl_output node for an agl_input routing group */
	if (type == agl_input) {
		node = agl_node_create (u, NULL);
		node->direction = agl_output;
		node->implement = agl_device;
		node->visible = true;
		node->available = true;
		node->paname = pa_xstrdup (node_desc);
		 /* add to global nodeset */
		pa_idxset_put (nodeset->nodes, node, &node->index);
		rtg->node = node;
	} else {
		node = agl_node_create (u, NULL);
		node->direction = agl_input;
		node->implement = agl_device;
		node->visible = true;
		node->available = true;
		node->paname = pa_xstrdup (node_desc);
		 /* add to global nodeset */
		pa_idxset_put (nodeset->nodes, node, &node->index);
		rtg->node = node;
	}
	

	pa_hashmap_put (table, rtg->name, rtg);

	pa_log_debug ("routing group '%s' created", name);

	return rtg;
}

void agl_router_destroy_rtgroup (struct userdata *u, agl_direction type, const char *name)
{
	agl_router *router;
	agl_rtgroup *rtg;
	pa_hashmap *table;

	pa_assert (u);
	pa_assert (name);
	pa_assert_se (router = u->router);

	if (type == agl_input)
		table = router->rtgroups.input;
	else
		table = router->rtgroups.output;
	pa_assert (table);

	rtg = pa_hashmap_remove (table, name);
	if (!rtg) {
		pa_log_debug ("can't destroy routing group '%s': not found", name);
	} else {
		//rtgroup_destroy (u, rtg);
		pa_log_debug ("routing group '%s' destroyed", name);
	}
}


agl_behavior_type agl_behavior_type_from_str (const char *str)
{
	agl_behavior_type type;

	pa_assert (str);
	if (pa_streq (str, "Reject"))
		type = agl_behavior_type_reject;
	else if (pa_streq (str, "SwitchOff"))
		type = agl_behavior_type_switchoff;
	else if (pa_streq (str, "SwitchCork"))
		type = agl_behavior_type_swithcork;
	else if (pa_streq (str, "SwitchNull"))
		type = agl_behavior_type_switchnull;
	else if (pa_streq (str, "ConMute"))
		type = agl_behavior_type_conmute;
	else if (pa_streq (str, "ConDuck"))
		type = agl_behavior_type_conduck;
	else if (pa_streq (str, "ConMix"))
		type = agl_behavior_type_conmix;
	else
		type = agl_behavior_type_unknown;

	return type;
}

static const char *get_behavior_name (agl_behavior_type behavior)
{
	switch (behavior) {
		case agl_behavior_type_reject:
			return "Reject";
		case agl_behavior_type_switchoff:
			return "SwitchOff";
		case agl_behavior_type_swithcork:
			return "SwitchCork";
		case agl_behavior_type_switchnull:
			return "SwitchNull";
		case agl_behavior_type_conmute:
			return "ConMute";
		case agl_behavior_type_conduck:
			return "ConDuck";
		case agl_behavior_type_conmix:
			return "ConMix";
		default:
			return NULL;
	}
}

	/*
	hashmap       | key         |  value
	--------------+-------------+----------
	behaviormap   | triggercase |  casemap
	casemap       | initcase    |  behavior
	*/
void agl_router_add_behavior_matrix(struct userdata *u, const char *initcase, const char *triggercase, const char *behavior)
{
	agl_router *router;
	agl_ucm_type ucm_type;
	pa_hashmap *casemap;

	pa_assert (u);
	pa_assert_se (router = u->router);

	ucm_type = agl_ucm_type_from_str(triggercase);

	if (ucm_type != agl_ucm_type_unknown)
		casemap = router->behaviormatrix.casemap[ucm_type];
	else {
		pa_log("Unknown ucm_verb !");
		return;
	}
	pa_hashmap_put (casemap, (void *)initcase, (void *)behavior);
	pa_hashmap_put (router->behaviormatrix.behaviormap, (void *)triggercase, casemap);

	pa_log_debug ("initcase is '%s' triggercase is '%s', behavior is '%s'", initcase, triggercase, behavior);
	return;
}

agl_behavior_type agl_router_get_behavior_type(struct userdata *u, const char *initcase, const char *triggercase)
{
	agl_router *router;
	const char *behavior;
	agl_behavior_type behavior_type;

	pa_assert (u);
	pa_assert_se (router = u->router);

	behavior = pa_hashmap_get(pa_hashmap_get (router->behaviormatrix.behaviormap, triggercase), initcase);
	behavior_type = agl_behavior_type_from_str(behavior);

	return behavior_type;
}

bool agl_router_assign_class_to_rtgroup (struct userdata *u, agl_node_type class, uint32_t zone, agl_direction type, const char *name)
{
	agl_router *router;
	pa_hashmap *rtable;
	agl_rtgroup ***classmap;
	agl_rtgroup **zonemap;
	const char *classname;
	const char *direction;
	agl_rtgroup *rtg;
	agl_zone *rzone;

	pa_assert (u);
	pa_assert (zone < AGL_ZONE_MAX);
	pa_assert (type == agl_input || type == agl_output);
	pa_assert (name);
	pa_assert_se (router = u->router);

	if (type == agl_input) {
		rtable = router->rtgroups.input;
		classmap = router->classmap.input;
	} else {
		rtable = router->rtgroups.output;
		classmap = router->classmap.output;
	}

	if (class < 0 || class >= router->maplen) {
		pa_log_debug ("Cannot assign class to routing group '%s': "
			      "id %d out of range (0 - %zu)",
			      name, class, router->maplen);
		return false;
	}

	classname = agl_node_type_str (class); /* "Player", "Radio"... */
	direction = agl_node_direction_str (type); /* "input", "output" */

	rtg = pa_hashmap_get (rtable, name);
	if (!rtg) {
		pa_log_debug ("Cannot assign class to routing group '%s': "
			      "router group not found", name);
		return false;
	}

	zonemap = classmap[zone];
	if (!zonemap) {
		zonemap = pa_xnew0 (agl_rtgroup *, router->maplen);
		classmap[zone] = zonemap;
	}

	zonemap[class] = rtg;

	 /* try to get zone name for logging, if fails, only print id number */
	rzone = agl_zoneset_get_zone_by_index (u, zone);
	if (rzone) {
		pa_log_debug ("class '%s'@'%s' assigned to routing group '%s'",
			      classname, rzone->name, name); 
	} else {
		pa_log_debug ("class '%s'@zone%d assigned to routing group '%s'",
			      classname, zone, name);
	}

	return true;
}

agl_rtgroup * agl_router_get_rtgroup_from_class (struct userdata *u, agl_node_type class, uint32_t zone, agl_direction type)
{
	agl_router *router;
	pa_hashmap *rtable;
	agl_rtgroup ***classmap;
	agl_rtgroup **zonemap;
	agl_rtgroup * rtg;

	pa_assert (u);
	pa_assert_se (router = u->router);
	pa_assert (class >= 0 && class < router->maplen);
	pa_assert (zone < AGL_ZONE_MAX);
	pa_assert (type == agl_input || type == agl_output);

	if (type == agl_input) {
		rtable = router->rtgroups.input;
		classmap = router->classmap.input;
	} else {
		rtable = router->rtgroups.output;
		classmap = router->classmap.output;
	}

	zonemap = classmap[zone];
	rtg = zonemap[class];

	return rtg;
}

void agl_router_assign_class_priority (struct userdata *u, agl_node_type class, int priority)
{
	agl_router *router;
	int *priormap;

	pa_assert (u);
	pa_assert_se (router = u->router);
	pa_assert_se (priormap = router->priormap);

	if (class > 0 && class < router->maplen) {
		pa_log_debug ("assigning priority %d to class '%s'",
			      priority, agl_node_type_str (class));
		priormap[class] = priority;
	}
}

int agl_router_get_node_priority (struct userdata *u, agl_node *node)
{
	agl_router *router;
	int class;

	pa_assert (u);
	pa_assert (node);
	pa_assert_se (router = u->router);

	class = node->type;

	if (class < 0 || class >= (int)router->maplen)
		return 0;

	return router->priormap[class];
}

bool agl_router_apply_node_priority_effect (struct userdata *u, agl_node *node, bool new)
{
	agl_router *router;
	agl_rtgroup *rtg;
	agl_nodeset *nodeset;
	agl_node *n;
	pa_sink *sink;
	int priority;
	uint32_t index;

	pa_assert (u);
	pa_assert (node);
	pa_assert_se (router = u->router);
	pa_assert_se (nodeset = u->nodeset);

	rtg = agl_router_get_rtgroup_from_class(u, node->type, 0, node->direction);

	/* now let us compare priorities, and apply effect if needed */
	/* "new" case */
	if (new) {
		priority = agl_router_get_node_priority (u, node);
		PA_IDXSET_FOREACH(n, nodeset->nodes, index) {
			if (n->nullsink && (priority > agl_router_get_node_priority (u, n))) {
				sink = agl_utils_get_null_sink (u, n->nullsink);
				if (sink) {
					/* do we have a custom effect ? otherwise, just mute it */
					if (rtg && rtg->effect)
						rtg->effect (u, rtg, n, new);
					else
						pa_sink_set_mute (sink, new, false);
				}
			}
		}
	} else {
	/* "old" case */
		if (!agl_node_has_highest_priority (u, node))
			return true;
		PA_IDXSET_FOREACH(n, nodeset->nodes, index) {
			if (n->nullsink) {
				sink = agl_utils_get_null_sink (u, n->nullsink);
				if (sink) {
					/* do we have a custom effect ? otherwise, just unmute it */
					if (rtg && rtg->effect)
						rtg->effect (u, rtg, n, new);
					else
						pa_sink_set_mute (sink, new, false);
				}
			}
		}
	}

	return true;
}

void agl_router_register_node (struct userdata *u, agl_node *node)
{
	agl_router *router;
	agl_rtgroup *rtg;

	pa_assert (u);
	pa_assert (node);
	pa_assert_se (router = u->router);

	/* we try to discover node routing group from the configuration, "Phone" for instance,
	 * see defaults in "config.c. Otherwise we just say NULL, a.k.a. default */
	rtg = agl_router_get_rtgroup_from_class(u, node->type, 0, node->direction);

	if (node->direction == agl_input) {
		if (rtg)
			implement_route (u, node, rtg->node, agl_utils_new_stamp ());
		else
			return;
	} else {
		if (rtg)
			implement_route (u, node, rtg->node, agl_utils_new_stamp ());
		else
			return;
	}
}

void agl_router_unregister_node (struct userdata *u, agl_node *node)
{
	pa_assert (u);
	pa_assert (node);

	remove_routes (u, node, NULL, agl_utils_new_stamp ());
}

agl_node *agl_router_make_prerouting (struct userdata *u, agl_node *data)
{
	agl_router *router;
	int priority;
	static bool done_prerouting;
	uint32_t stamp;
	agl_node *start, *end;
	agl_node *target;

	pa_assert (u);
	pa_assert_se (router = u->router);
	pa_assert_se (data->implement == agl_stream);

	//priority = node_priority (u, data);

	done_prerouting = false;
	target = NULL;
	stamp = agl_utils_new_stamp ();

	//make_explicit_routes (u, stamp);

	//pa_audiomgr_delete_default_routes(u);

	AGL_DLIST_FOR_EACH_BACKWARDS(agl_node, rtprilist, start, &router->nodlist) {
		//if ((start->implement == agl_device) &&
		//    (!start->loop))	/* only manage looped real devices */
		//	continue;

		/*if (priority >= node_priority (u, start)) {
			target = find_default_route (u, data, stamp);
			if (target)
				implement_preroute (u, data, target, stamp);
			else
				done_prerouting = true;
		}*/

		if (start->stamp >= stamp)
			continue;

		//end = find_default_route (u, start, stamp);
		//if (end)
		//	implement_default_route(u, start, end, stamp);
	}

	if (!done_prerouting) {
		pa_log_debug ("Prerouting failed, trying to find default route as last resort");

		//target = find_default_route (u, data, stamp);
		//if (target)
		//	implement_preroute (u, data, target, stamp);
	}

	return target;
}

void agl_router_make_routing (struct userdata *u)
{
	agl_router *router;
	static bool ongoing_routing;	/* true while we are actively routing */
	uint32_t stamp;
	agl_node *start, *end;

	pa_assert (u);
	pa_assert_se (router = u->router);

	if (ongoing_routing)		/* already routing, canceling */
		return;
	ongoing_routing = true;
	stamp = agl_utils_new_stamp ();

	pa_log_debug("stamp for routing: %d", stamp);

	// make_explicit_routes (u, stamp);

	// pa_audiomgr_delete_default_routes (u);

	AGL_DLIST_FOR_EACH_BACKWARDS(agl_node, rtprilist, start, &router->nodlist) {
		//if ((start->implement == agl_device) &&
		//   (!start->loop))	/* only manage looped real devices */
		//	continue;

		if (start->stamp >= stamp)
			continue;

		end = find_default_route (u, start, stamp);
		if (end)
			implement_default_route (u, start, end, stamp);
	}

	// pa_audiomgr_send_default_routes (u);

	ongoing_routing = false;
}

void implement_default_route (struct userdata *u,
                              agl_node *start, agl_node *end,
                              uint32_t stamp)
{
	if (start->direction == agl_input)
		agl_switch_setup_link (u, start, end);
	else
		agl_switch_setup_link (u, end, start);
}

/* add a connection to the connect list, next to the position prev */
static agl_dlist *conn_add (agl_dlist *prev, agl_dlist *curr)
{
	curr->next = prev->next;
	prev->next = curr;
	curr->prev = prev;
	curr->next->prev = curr;
	return curr;
}

/* remove a connection from the connect list */
static agl_dlist *conn_del (agl_connection *conn)
{
	agl_dlist *curr = &conn->link;
	agl_dlist *prev = curr->prev;

	AGL_DLIST_UNLINK(agl_connection, link, conn);
	pa_xfree(conn);
	return prev;
}

/* find the foreground connection in the connect list */
static agl_connection *conn_find_foreground(struct userdata *u)
{
	agl_router *router;
	agl_dlist *head;
	agl_connection *pos;

	pa_assert_se (router = u->router);
	pa_assert_se (head = &router->connlist);

	if (head->next == head)
		return NULL;

	return AGL_LIST_RELOCATE(agl_connection, link, head->prev);
}

/* find the connection with specified node index in the connect list */
static agl_connection *conn_find_idx(struct userdata *u, uint32_t idx)
{
	agl_router *router;
	agl_dlist *head;
	agl_connection *pos, *conn;

	pa_assert_se (router = u->router);
	pa_assert_se (head = &router->connlist);

	if (head->next == head)
		return NULL;

	AGL_DLIST_FOR_EACH_BACKWARDS(agl_connection, link, pos, head) {
		if (idx == pos->node->index)
			return pos;

		conn = pos->guest;
		while (conn) {
			if (idx == conn->node->index)
				return conn;
			conn = conn->guest;
		}
	}
	return NULL;
}

/* find the connection ahead of target connection in the connect list */
static agl_connection *conn_find_prev(struct userdata *u, agl_connection *conn)
{
	agl_router *router;
	agl_dlist *head;
	agl_connection *pos;

	pa_assert_se (router = u->router);
	pa_assert_se (head = &router->connlist);

	if (head->next == head || conn->link.prev == head)
		return NULL;

	pos = AGL_LIST_RELOCATE(agl_connection, link, conn->link.prev);
	if (&pos->link == head || pos == conn)
		return NULL;
	else
		return pos;
}

/* find the position of the last connection with specified priority in the connect list */
static agl_dlist *find_position_prio(struct userdata *u, int prio)
{
	agl_router *router;
	agl_dlist *head;
	agl_connection *pos;

	pa_assert_se (router = u->router);
	pa_assert_se (head = &router->connlist);

	if (head->next == head)
		return head;

	AGL_DLIST_FOR_EACH_BACKWARDS(agl_connection, link, pos, head) {
		if (prio >= agl_router_get_node_priority(u, pos->node))
			return &pos->link;
	}
	return head;
}

static bool behavior_is_valid(agl_behavior_type behavior)
{
	switch (behavior) {
		case agl_behavior_type_reject:
		case agl_behavior_type_switchoff:
		case agl_behavior_type_swithcork:
		case agl_behavior_type_switchnull:
		case agl_behavior_type_conmute:
		case agl_behavior_type_conduck:
		case agl_behavior_type_conmix:
			return true;
		default:
			return false;
	}
}

static bool behavior_is_concurrent(agl_behavior_type behavior)
{
	switch (behavior) {
		case agl_behavior_type_conmute:
		case agl_behavior_type_conduck:
		case agl_behavior_type_conmix:
			return true;
		default:
			return false;
	}
}

static bool behavior_is_switch(agl_behavior_type behavior)
{
	switch (behavior) {
		case agl_behavior_type_switchoff:
		case agl_behavior_type_swithcork:
		case agl_behavior_type_switchnull:
			return true;
		default:
			return false;
	}
}

static agl_behavior_type get_behavior (struct userdata *u,
		agl_connection *init, agl_connection *trigger)
{
	const char *init_case = NULL, *trigger_case = NULL;
	agl_behavior_type behavior;

	init_case = agl_node_usecase_str(init->node);
	trigger_case = agl_node_usecase_str(trigger->node);
	pa_log_debug("%s init case %s, trigger case %s",
			__func__, init_case, trigger_case);

	if (!init_case || !trigger_case)
		return agl_behavior_type_unknown;

	behavior = agl_router_get_behavior_type(u, init_case, trigger_case);

	return behavior;
}

static void link_behavior(agl_rtgroup_effect_t *func, agl_behavior_type behavior)
{
	switch (behavior) {
		case agl_behavior_type_switchoff:
			*func = agl_router_switchoff_effect;
			break;
		case agl_behavior_type_swithcork:
			*func = agl_router_cork_effect;
			break;
		case agl_behavior_type_switchnull:
			*func = agl_router_switch_effect;
			break;
		case agl_behavior_type_conmute:
			*func = agl_router_mute_effect;
			break;
		case agl_behavior_type_conduck:
			*func = agl_router_duck_effect;
			break;
		case agl_behavior_type_conmix:
			*func = agl_router_mix_effect;
			break;
		default:
			break;
	}
}

static void sync_strlist_behavior(struct userdata *u, agl_rtgroup_effect_t behavior, agl_connection *conn, bool up)
{
	agl_node *strlink_node;
	agl_dlist *strlink;

	pa_assert_se (strlink = &conn->strlist);

	behavior(u, NULL, conn->node, up);

	while(strlink->next != &conn->strlist) {
		strlink = strlink->next;
		strlink_node = AGL_LIST_RELOCATE(agl_node, strlink, strlink);
		behavior(u, NULL, strlink_node, up);
	}
}

static void assign_effect(struct userdata *u, agl_connection *conn, agl_behavior_type behavior)
{
	if (behavior > agl_behavior_type_conmix)
		behavior = agl_behavior_type_conmix;
	else if (behavior < agl_behavior_type_conmute)
		behavior = agl_behavior_type_conmute;
	conn->behavior = behavior;

	link_behavior(&conn->effect, behavior);
	conn->effect_name = get_behavior_name(behavior);

	if (conn->effect)
		sync_strlist_behavior(u, conn->effect, conn, 1);
}

static void assign_suppress(struct userdata *u, agl_connection *conn, agl_connection *target, agl_behavior_type behavior)
{
	if (behavior > agl_behavior_type_switchnull)
		behavior = agl_behavior_type_switchnull;
	else if (behavior < agl_behavior_type_switchoff)
		behavior = agl_behavior_type_switchoff;

	link_behavior(&target->suppress, behavior);
	target->suppress_name = get_behavior_name(behavior);

	if (target->suppress)
		sync_strlist_behavior(u, target->suppress, target, 1);

	if (behavior != agl_behavior_type_switchoff) {
		conn->guest = target;
		target->host = conn;
		AGL_DLIST_UNLINK(agl_connection, link, target);
	} else {
		conn_del(target);
	}
}

static void update_behavior_list (struct userdata *u, agl_connection *end, int fg)
{
	agl_router *router;
	agl_dlist *head;
	agl_connection *pos, *n;
	agl_behavior_type behavior;

	pa_assert_se (router = u->router);
	pa_assert_se (head = &router->connlist);

	if (head->next == head)
		return;

	AGL_DLIST_FOR_EACH_SAFE(agl_connection, link, pos, n, head) {
		if (pos == end)
			break;
		behavior = get_behavior(u, pos, end);
		if (behavior_is_switch(behavior)) {
			assign_suppress (u, end, pos, behavior);
		}
		else if (behavior_is_concurrent(behavior) && fg) {
			if (pos->effect)
				sync_strlist_behavior (u, pos->effect, pos, 0);
			assign_effect (u, pos, behavior);
		}
	}

	pa_log_debug("list behavior updated");
}

void print_route (struct userdata *u)
{
	agl_router *router;
	agl_dlist *head, *strlink;
	agl_connection *pos, *n, *conn;
	agl_node *strlink_node;
	int i = 0, str_idx[MAX_STREAMLIST];

	pa_assert_se (router = u->router);
	pa_assert_se (head = &router->connlist);

	pa_log_debug("%s begin:", __func__);

	if (head->next == head)
		goto exit;

	AGL_DLIST_FOR_EACH_SAFE(agl_connection, link, pos, n, head) {
		/* Update the connection status and print */
		if (!pos->effect)
			pos->effect_name = NULL;
		if (!pos->suppress)
			pos->suppress_name = NULL;

		/* find the sream list node and print */
		i = 0;
		strlink = &pos->strlist;
		while(strlink->prev != &pos->strlist) {
		strlink = strlink->prev;
		strlink_node = AGL_LIST_RELOCATE(agl_node, strlink, strlink);
		if (i < MAX_STREAMLIST)
			str_idx[i] = strlink_node->index;
		else
			pa_log_debug("Only support print stream list max number for %d", MAX_STREAMLIST);
		i++;
		}

		switch (i){
		case 0:
			pa_log_debug("%s connection node:(%d:%s,%s,%s)", __func__,
				pos->idx, pos->usecase,
				pos->effect_name, pos->suppress_name);
			break;
		case 1:
			pa_log_debug("%s connection node:(%d,%d:%s,%s,%s)", __func__,
				pos->idx, str_idx[0], pos->usecase,
				pos->effect_name, pos->suppress_name);
			break;
		case 2:
			pa_log_debug("%s connection node:(%d,%d,%d:%s,%s,%s)", __func__,
				pos->idx, str_idx[0],  str_idx[1], pos->usecase,
				pos->effect_name, pos->suppress_name);
			break;
		case 3:
		default:
			pa_log_debug("%s connection node:(%d,%d,%d,%d:%s,%s,%s)", __func__,
				pos->idx, str_idx[0],  str_idx[1],   str_idx[2], pos->usecase,
				pos->effect_name, pos->suppress_name);
			break;
		}

		/* find the suppressed connection and print */
		conn = pos->guest;
		while (conn) {
			if (!conn->effect)
				conn->effect_name = NULL;
			if (!conn->suppress)
				conn->suppress_name = NULL;

			/* find the suppressed sream list node and print */
			i = 0;
			strlink = &conn->strlist;
			while(strlink->prev != &conn->strlist) {
				strlink = strlink->prev;
				strlink_node = AGL_LIST_RELOCATE(agl_node, strlink, strlink);
				if (i < MAX_STREAMLIST)
					str_idx[i] = strlink_node->index;
				else
					pa_log_debug("Only support print stream list max number for %d", MAX_STREAMLIST);
				i++;
			}

			switch (i){
			case 0:
				pa_log_debug("%s >>>>>>>> suppressed node: (%d:%s -> %d:%s,%s,%s)", __func__,
					conn->host->idx, conn->host->usecase,
					conn->idx, conn->usecase,
					conn->effect_name, conn->suppress_name);
				break;
			case 1:
				pa_log_debug("%s >>>>>>>> suppressed node: (%d:%s -> %d,%d:%s,%s,%s)", __func__,
					conn->host->idx, conn->host->usecase,
					conn->idx, str_idx[0], conn->usecase,
					conn->effect_name, conn->suppress_name);
				break;
			case 2:
				pa_log_debug("%s >>>>>>>> suppressed node: (%d:%s -> %d,%d,%d:%s,%s,%s)", __func__,
					conn->host->idx, conn->host->usecase,
					conn->idx, str_idx[0],  str_idx[1], conn->usecase,
					conn->effect_name, conn->suppress_name);
				break;
			case 3:
			default:
				pa_log_debug("%s >>>>>>>> suppressed node: (%d:%s -> %d,%d,%d,%d:%s,%s,%s)", __func__,
					conn->host->idx, conn->host->usecase,
					conn->idx, str_idx[0],  str_idx[1],   str_idx[2], conn->usecase,
					conn->effect_name, conn->suppress_name);
				break;
			}
			conn = conn->guest;
		}

	}
exit:
	pa_log_debug("%s end.", __func__);
}

static void setup_link (struct userdata *u, agl_node *start, agl_node *end)
{
	if (start->direction == agl_input) {
		agl_switch_setup_link (u, start, end);
	} else {
		agl_switch_setup_link (u, end, start);
	}
	print_route(u);
}

static void teardown_link (struct userdata *u, agl_node *start, agl_node *end)
{
	if (start->direction == agl_input) {
		agl_switch_teardown_link (u, start, end);
	} else {
		agl_switch_teardown_link (u, end, start);
	}
	print_route(u);
}

void implement_route (struct userdata *u,
                      agl_node *start, agl_node *end,
                      uint32_t stamp)
{
	agl_router *router;
	agl_connection *foreground, *trigger, *target, *pos, *n;
	agl_dlist *head, *posi;
	agl_behavior_type behavior, behavior_target;
	int foreground_prio, trigger_prio;

	pa_assert_se (router = u->router);
	pa_assert_se (head = &router->connlist);

	/* if the trigger and exist connection are from same client, only sync the effect/suppress and setup link, skip apply any behavior*/
	AGL_DLIST_FOR_EACH_SAFE(agl_connection, link, pos, n, head) {
		if (start->client->index == pos->node->client->index) {
			/* not connect to the connlist, but to the strlist*/
			pa_log_debug("behavior is already applied for multi-stream usecase");
			posi = &pos->strlist;
			conn_add(posi, &start->strlink);

			if (pos->effect)
				pos->effect(u, NULL, start, 1);
			if (pos->suppress)
				pos->suppress(u, NULL, start, 1);

			setup_link(u, start, end);
			return;
		}
	}

	trigger = pa_xnew0(agl_connection, 1);
	AGL_DLIST_INIT(trigger->strlist);
	trigger->node = start;
	trigger->idx = start->index;
	trigger->usecase = agl_node_usecase_str(start);

	pa_log_debug("%s get foreground", __func__);
	foreground = conn_find_foreground(u);

	/* playback path */
	if (foreground == NULL) {
		/* the connection list is empty */
		pa_log_debug("%s branch [1]", __func__);
		conn_add(head, &trigger->link);
		setup_link(u, start, end);
	} else {
		/* the connection list is not empty */

		/* get behavior */
		behavior = get_behavior(u, foreground, trigger);
		pa_log_debug("%s behavior %d", __func__, behavior);
		if (!behavior_is_valid(behavior))
			return;
		/* get priority */
		foreground_prio = agl_router_get_node_priority(u, foreground->node);
		trigger_prio = agl_router_get_node_priority(u, trigger->node);
		pa_log_debug("%s foreground_prio %d trigger_prio %d",
				__func__, foreground_prio, trigger_prio);

		if (!behavior_is_concurrent(behavior)) {
			/* non-concurrent: reject, switch off, switch null, switch cork,
			 * apply suppress */
			if (trigger_prio >= foreground_prio) {
				/* foreground priority is equal or lower, apply suppress */
				pa_log_debug("%s branch [3]", __func__);
				/* add connect as new foreground and suppress foreground */
				posi = &foreground->link;
				conn_add(posi, &trigger->link);
				assign_suppress(u, trigger, foreground, behavior);

				/* update whole conn list behavior since foreground changed */
				update_behavior_list(u, trigger, 1);
				setup_link(u, start, end);
			} else {
				/* trigger priority is lower, reject */
				pa_log_debug("%s branch [2]", __func__);
				agl_router_reject_effect (u, NULL, trigger->node, 1);
			}
		} else {
			/* concurrent: mute, duck, mix, apply effect */
			if (trigger_prio >= foreground_prio) {
				/* foreground priority is equal or lower, apply effect */
				pa_log_debug("%s branch [6]", __func__);
				/* add connect as new foreground */
				posi = &foreground->link;
				conn_add(posi, &trigger->link);

				/* update whole conn list behavior since foreground changed */
				update_behavior_list(u, trigger, 1);
				setup_link(u, start, end);
			} else {
				/* trigger priority is lower, apply effect */
				posi = find_position_prio(u, trigger_prio);
				conn_add(posi, &trigger->link);
				assign_effect(u, trigger, behavior);

				/* find target that conflict with trigger, and switch/apply suppress */
				target = conn_find_prev(u, trigger);
				if (!target || trigger_prio != agl_router_get_node_priority(u, target->node)) {
					/* not found target, return */
					pa_log_debug("%s branch [4]", __func__);
				} else {
					/* found target, switch/apply suppress */
					pa_log_debug("%s branch [5]", __func__);
					behavior_target = get_behavior(u, target, trigger);
					pa_log_debug("%s behavior_target %d", __func__, behavior_target);
					if (behavior_is_switch(behavior_target))
						/* target priority is equal or lower, apply suppress */
						assign_suppress(u, trigger, target, behavior_target);
				}

				/* update conn list behavior before trigger case position of added middle list node */
				update_behavior_list(u, trigger, 0);
				setup_link(u, start, end);
			}
		}
	}
}

agl_node *find_default_route (struct userdata *u, agl_node *start, uint32_t stamp)
{
	/* TODO */
	return NULL;
}

void remove_routes (struct userdata *u, agl_node *start, agl_node *end, uint32_t stamp)
{
	agl_router *router;
	agl_dlist *head, *strhead, *posi;
	agl_connection *target, *conn, *pos, *n, *foreground;
	agl_behavior_type behavior;

	pa_assert_se (router = u->router);
	pa_assert_se (head = &router->connlist);

	target = conn_find_idx(u, start->index);

	/* For below cases, no need to update whole connection list behavior,
	 * only need to sync related status before teardown node link:
	 * 1) a stream list node;
	 * 2) a connection list node but stream list is not empty;
	 * 3) a suppressed node. */

	/* can't find target node on connection list */
	if (!target) {
		/* If the node is on a streamlist, just remove target and teardown link */
		AGL_DLIST_FOR_EACH_SAFE(agl_connection, link, pos, n, head) {
			if (start->client->index == pos->node->client->index) {
				pa_log_debug("find the node on a stream list, only teardown link!");
				AGL_DLIST_UNLINK(agl_node, strlink, start);
				teardown_link(u, start, end);
				return;
			}
			conn = pos->guest;
			while (conn) {
				if (start->client->index == conn->node->client->index) {
					pa_log_debug("find the node on a suppressed stream list, only teardown link!");
					AGL_DLIST_UNLINK(agl_node, strlink, start);
					teardown_link(u, start, end);
					return;
				}
				conn = conn->guest;
		}
		}

		pa_log_debug("%s not find target node on connection list or srteam list. Reject/Switch off applied?", __func__);
		return;
	}

	/* if target's stream list is no empty, just teardown link and move next stream list node to connection */
	pa_assert_se (strhead = &target->strlist);
	if(strhead->next != &target->strlist) {
		pa_log_debug("still have node on this connection stream list, only teardown link!");
		strhead = strhead->next;
		AGL_DLIST_UNLINK(agl_connection, strlist, target);
		target->node = AGL_LIST_RELOCATE(agl_node, strlink, strhead);
		target->idx = target->node->index;
		target->usecase = agl_node_usecase_str(target->node);
		teardown_link(u, start, end);
		return;
	}

	/* if target has a host, it's suppressed connection node, just teardown link */
	if (target->host) {
		if (target->guest) {
			/* notify the host that guest is updated */
			target->host->guest = target->guest;
			/* notify the guest that host is updated */
			target->guest->host = target->host;
		} else {
			/* notify the host that no suppress guest now */
			target->host->guest = NULL;
		}

		pa_xfree(target);
		teardown_link(u, start, end);
		return;
	}

	/* For the node on the connection list, need to do:
	 * 1) resume suppress of suppressed node, and add it into connection list;
	 * 2) resume effect of previous node if target is foreground;
	 * 3) update list behavior if target is foreground;
	 * 4) only need re-assign behavior between new added suppressed node and foreground node if target is not forgground */

	/* If target has a guest but no host, it's on the connection list */
	if (!target->host && target->guest) {
		/* resume guest */
		if (target->guest->suppress)
			sync_strlist_behavior (u, target->guest->suppress, target->guest, 0);
		/* notify the guest is resumed */
		target->guest->host = NULL;
		target->guest->suppress = NULL;

		/* add the guest to connection list */
		posi = find_position_prio(u, agl_router_get_node_priority(u, target->guest->node));
		conn_add (posi, &target->guest->link);
    }

	/* if target is foreground */
	if (target->link.next == head) {
		pa_log("%s branch [3]", __func__);
		/* resume the effect of previous connection node */
		conn = conn_find_prev(u, target);
		if (conn && conn->effect) {
			sync_strlist_behavior (u, conn->effect, conn, 0);
			conn->effect = NULL;
		}
		conn_del(target);

		/* find new foreground after target is deleted */
		foreground = conn_find_foreground(u);
		/* update whole conn list behavior since foreground changed */
		if (foreground)
			update_behavior_list(u, foreground, 1);
		else
			pa_log_error("foreground is NULL");
	} else {
		/* update new added conn and forground behavior */
		if (target->guest) {
			pa_log("%s branch [2]", __func__);
			conn = target->guest;
			conn_del(target);

			/* find new foreground after target is deleted */
			foreground = conn_find_foreground(u);
			if (foreground && (foreground->idx != conn->idx)) {
				behavior = get_behavior(u, foreground, conn);
				if (behavior_is_switch(behavior)) {
					assign_suppress(u, foreground, conn, behavior);
				}
				else {
					assign_effect(u, conn, behavior);
				}
			} else
				pa_log("bug! target guest shouldn't be foreground when the target itself is not foreground!");
		} else {
			/* only need to delete conn if target is not foreground and no suppress other node */
			pa_log("%s branch [1]", __func__);
			conn_del(target);
		}
	}

	teardown_link(u, start, end);
}

agl_acceptset *agl_acceptset_init (void)
{
	agl_acceptset *as;

	as = pa_xnew0 (agl_acceptset, 1);
	as->accepts = pa_idxset_new (pa_idxset_string_hash_func,
		                         pa_idxset_string_compare_func);

	return as;
}

void agl_acceptset_done (agl_rtgroup *rtg)
{
agl_acceptset *as;

	if (rtg && (as = rtg->acceptset)) {
		pa_idxset_free (as->accepts, NULL);

		free(as);
	}
}

int agl_acceptset_add_effect (agl_acceptset *acceptset, const char *name)
{
	agl_acceptset *as;
	agl_accept *accept;

	pa_assert (name);
	pa_assert_se (as = acceptset);

	accept = pa_xnew (agl_accept, 1);

	pa_idxset_put (as->accepts, accept, &accept->index);
	accept->name = pa_xstrdup (name);

	return 0;
}

bool agl_find_accept_fct(agl_acceptset *acceptset, const char *effect_fct)
{
	 uint32_t index;
	 agl_accept *accept_list;

		 PA_IDXSET_FOREACH(accept_list, acceptset->accepts, index) {
			pa_log("accept list has(%s) effect.",  accept_list->name);
			if(pa_streq(effect_fct, accept_list->name))
				return true;
		 }
	return false;
}
