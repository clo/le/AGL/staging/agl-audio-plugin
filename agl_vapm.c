/***
agl_vapm.c -- Virtualized audio policy manager is a module that loaded
by agl-audio-plugin. It used to register, deregister, request and
release permission to PVM VAPM backend server for virtualized audio
policy controls.

Copyright (c) 2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/

#include <dlfcn.h>
#include <pthread.h>
#include "agl_vapm.h"
#include "utils.h"

static agl_vapm_node *g_vapm_head;

agl_vapm *agl_vapm_init (struct userdata *u)
{
	agl_vapm *vapm;

	pa_assert (u);

	vapm = pa_xnew0 (agl_vapm, 1);

	vapm->hdl = dlopen ("libvapm_lib.so", RTLD_NOW);
	if (!vapm->hdl)
		goto err1;
	vapm->init = dlsym (vapm->hdl, "vapm_fe_init");
	vapm->deinit = dlsym (vapm->hdl, "vapm_fe_deinit");
	vapm->register_client = dlsym (vapm->hdl, "vapm_register_client");
	vapm->deregister_client = dlsym (vapm->hdl, "vapm_deregister_client");
	vapm->request_permission = dlsym (vapm->hdl, "vapm_request_permission");
	vapm->release_permission = dlsym (vapm->hdl, "vapm_release_permission");
	if (!vapm->init || !vapm->deinit ||
		!vapm->register_client || !vapm->deregister_client ||
		!vapm->request_permission || !vapm->release_permission)
		goto err2;
	vapm->cb = agl_vapm_callback;
	pthread_mutex_init(&vapm->lock, NULL);
	pthread_mutex_lock(&vapm->lock);
	vapm->reg_count = 0;
	vapm->act_count = 0;
	pthread_mutex_unlock(&vapm->lock);

	return vapm;
err2:
	dlclose (vapm->hdl);
err1:
	pa_xfree(vapm);
	return NULL;
}

void agl_vapm_done (struct userdata *u)
{
	agl_vapm *vapm;

	if (u && (vapm = u->vapm)) {
		pthread_mutex_destroy(&vapm->lock);
		dlclose (vapm->hdl);
		pa_xfree(vapm);
		u->vapm = NULL;
	}
}

int32_t agl_vapm_create_fe_service (struct userdata *u)
{
	pa_assert (u);
	pa_assert (u->vapm);
	pa_assert (u->vapm->init);

	return u->vapm->init();
}

static agl_vapm_node *agl_vapm_alloc_node (void)
{
	return (agl_vapm_node *)pa_xnew0 (agl_vapm_node, 1);
}

static void agl_vapm_free_node (agl_vapm_node *vnode)
{
	pa_xfree(vnode);
}

static int32_t agl_vapm_put_node_to_list (agl_vapm_node *vnode)
{
	agl_vapm_node *tmp = g_vapm_head;

	if (!tmp) {
		g_vapm_head = vnode;
		return 0;
	}

	while (tmp->next) {
		tmp = tmp->next;
	}
	tmp->next = vnode;
	vnode->prev = tmp;

	return 0;
}

static agl_vapm_node *agl_vapm_remove_node_from_list (agl_node *node)
{
	agl_vapm_node *tmp = g_vapm_head;

	while (tmp) {
		if (tmp->node->index == node->index) {
			if (tmp->prev && tmp->next) {
				tmp->prev->next = tmp->next;
				tmp->next->prev = tmp->prev;
			} else if (tmp->prev && !tmp->next) {
				tmp->prev->next = NULL;
			} else if (!tmp->prev && tmp->next) {
				tmp->next->prev = NULL;
				g_vapm_head = tmp->next;
			} else {
				g_vapm_head = NULL;
			}
			return tmp;
		}
		tmp = tmp->next;
	}

	return NULL;
}

int32_t agl_vapm_register_client (struct userdata *u, agl_node *node)
{
	int32_t rc = 0;

	pa_assert (u);
	pa_assert (u->vapm);
	pa_assert (u->vapm->register_client);

	pthread_mutex_lock(&u->vapm->lock);
	if (u->vapm->reg_count == 0) {
		/* not registered, register and add counter */
		u->vapm->handle = u->vapm->register_client (VAPM_VM_TYPE_2,
				u->vapm->cb);
		if (u->vapm->handle <= 0) {
			pa_log_debug ("Failed to register client");
			rc = -1;
		} else {
			u->vapm->reg_count = 1;
		}
	} else if (u->vapm->reg_count > 0) {
		/* already registered, add counter */
		u->vapm->reg_count++;
	} else {
		/* invalid counter value, return error */
		pa_log_debug ("Invalid VAPM reg count: %d", u->vapm->reg_count);
		rc = -1;
	}
	pthread_mutex_unlock(&u->vapm->lock);

	return rc;
}

int32_t agl_vapm_deregister_client (struct userdata *u, agl_node *node)
{
	int32_t rc = 0;

	pa_assert (u);
	pa_assert (u->vapm);
	pa_assert (u->vapm->deregister_client);

	pthread_mutex_lock(&u->vapm->lock);
	if (u->vapm->reg_count > 1) {
		/* reduce counter */
		u->vapm->reg_count--;
	} else if (u->vapm->reg_count == 1) {
		/* reduce counter and deregister */
		rc = u->vapm->deregister_client (u->vapm->handle);
		if (rc) {
			pa_log_debug ("Failed to deregister client");
			rc = -1;
		} else {
			u->vapm->reg_count = 0;
		}
	} else {
		/* invalid counter value, return error */
		pa_log_debug ("Invalid VAPM reg count: %d", u->vapm->reg_count);
		rc = -1;
	}
	pthread_mutex_unlock(&u->vapm->lock);

	return rc;
}

int32_t agl_vapm_request_permission (struct userdata *u, agl_node *node)
{
	agl_vapm_node *vnode;
	int32_t rc = 0;

	pa_assert (u);
	pa_assert (u->vapm);
	pa_assert (u->vapm->request_permission);

	vnode = agl_vapm_alloc_node();
	if (!vnode) {
		pa_log_debug ("Failed to allocate vapm node");
		return -1;
	}
	vnode->u = u;
	vnode->node = node;

	pthread_mutex_lock(&u->vapm->lock);
	if (u->vapm->act_count == 0) {
		/* not active, activate and add counter */
		rc = u->vapm->request_permission (u->vapm->handle);
		if (rc) {
			pa_log_debug ("Failed to request BE permission");
			agl_vapm_free_node(vnode);
			rc = -1;
		} else {
			u->vapm->act_count = 1;
			agl_vapm_put_node_to_list (vnode);
		}
	} else if (u->vapm->act_count > 0) {
		/* is active, add counter */
		u->vapm->act_count++;
		agl_vapm_put_node_to_list (vnode);
	} else {
		/* invalid counter value, return error */
		pa_log_debug ("Invalid VAPM act count: %d", u->vapm->act_count);
		agl_vapm_free_node(vnode);
		rc = -1;
	}
	pthread_mutex_unlock(&u->vapm->lock);

	return rc;
}

int32_t agl_vapm_release_permission (struct userdata *u, agl_node *node)
{
	agl_vapm_node *vnode;
	int32_t rc = 0;

	pa_assert (u);
	pa_assert (u->vapm);
	pa_assert (u->vapm->release_permission);

	pthread_mutex_lock(&u->vapm->lock);
	if (u->vapm->act_count > 1) {
		/* reduce counter */
		u->vapm->act_count--;
		vnode = agl_vapm_remove_node_from_list (node);
		if (vnode) {
			agl_vapm_free_node(vnode);
		} else { /* unlikely */
			pa_log_debug ("Failed to remove node from list");
		}
	} else if (u->vapm->act_count == 1) {
		/* reduce counter and deactivate */
		rc = u->vapm->release_permission (u->vapm->handle);
		if (rc) {
			pa_log_debug ("Failed to release BE permission");
			rc = -1;
		} else {
			u->vapm->act_count = 0;
			vnode = agl_vapm_remove_node_from_list (node);
			if (vnode) {
				agl_vapm_free_node(vnode);
			} else { /* unlikely */
				pa_log_debug ("Failed to remove node from list");
			}
		}
	} else {
		/* invalid counter value, return error */
		rc = -1;
	}
	pthread_mutex_unlock(&u->vapm->lock);

	return rc;
}

static void agl_vapm_duck_effect (struct userdata *u, agl_node *node,
				pa_sink_input *sin, bool enable)
{
	pa_cvolume cv;

	/* force set to 0 to avoid invalid restore */
	sin->save_volume = false;
	sin->save_muted = false;
	cv = *pa_sink_input_get_volume (sin, &cv, false);

	if (enable)
		pa_cvolume_dec (&cv, PA_VOLUME_NORM/2);
	else
		pa_cvolume_inc (&cv, PA_VOLUME_NORM/2);

	pa_sink_input_set_volume (sin, &cv, false, false);
}

static void agl_vapm_mute_effect (struct userdata *u, agl_node *node,
				pa_sink_input *sin, bool enable)
{
	pa_sink_input_set_mute(sin, enable, false);
}

static void agl_vapm_switch_off_effect (struct userdata *u, agl_node *node,
					pa_sink_input *sin, bool enable)
{
	pa_sink_input_send_event (sin, PA_STREAM_EVENT_RIGHT_LOST, NULL);
}

static void agl_vapm_apply_effect_all (agl_vapm_effect func, bool enable)
{
	agl_vapm_node *tmp = g_vapm_head;
	pa_core *core;
	agl_node *node;
	pa_sink_input *sin;

	while (tmp) {
		if (!tmp->u || !tmp->u->core || !tmp->node) {
			pa_log_error("Invalid vapm node to apply effect");
			break;
		}
		core = tmp->u->core;
		node = tmp->node;
		sin = pa_idxset_get_by_index(core->sink_inputs, node->paidx);
		if (!sin) {
			pa_log_error("can't find sink input for '%s'", node->amname);
			break;
		}

		while (pa_sink_input_get_state(sin) == PA_SINK_INPUT_INIT);
		func(tmp->u, node, sin, enable);

		tmp = tmp->next;
	}
}

int32_t agl_vapm_callback (vapm_cb_param *param)
{
	pa_assert (param);

	switch (param->effect) {

	case VAPM_EFFECT_DUCK:
		pa_log_debug ("Effect callback: duck %s",
				(param->enable ? "enable" : "disable"));
		agl_vapm_apply_effect_all (agl_vapm_duck_effect, param->enable);
		break;

	case VAPM_EFFECT_MUTE:
		pa_log_debug ("Effect callback: mute %s",
				(param->enable ? "enable" : "disable"));
		agl_vapm_apply_effect_all (agl_vapm_mute_effect, param->enable);
		break;

	case VAPM_EFFECT_SOFF:
		pa_log_debug ("Effect callback: switchoff");
		agl_vapm_apply_effect_all (agl_vapm_switch_off_effect, 1);
		break;

	default:
		pa_log_debug ("Effect callback: Unsupported effect %d",
				param->effect);
		break;
	}

	return 0;
}
