/*
 * module-agl-audio -- PulseAudio module for providing audio routing support
 * (forked from "module-murphy-ivi" - https://github.com/otcshare )
 * Copyright (c) 2012, Intel Corporation.
 * Copyright (c) 2016, IoT.bzh
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St - Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 */
#ifndef paagluserdata
#define paagluserdata

#include <pulsecore/pulsecore-config.h>	/* required for "core.h" and "module.h" */
#include <pulsecore/core.h>
#include <pulsecore/module.h>
#include <alsa/use-case.h> /* for UCM */
#include <pulse/pulseaudio.h>
#ifdef ENABLE_VAPM
#include <vapm_lib/vapm.h>
#endif

#define AM_ID_INVALID 65535		/* invalid state in several places */


typedef struct agl_null_sink agl_null_sink;
typedef struct agl_null_source agl_null_source;

typedef struct agl_zoneset agl_zoneset;
typedef struct agl_nodeset agl_nodeset;
typedef struct agl_audiomgr agl_audiomgr;
typedef struct agl_routerif agl_routerif;
typedef struct agl_router agl_router;
typedef struct agl_discover agl_discover;
typedef struct agl_tracker agl_tracker;

typedef struct agl_nodeset_resdef agl_nodeset_resdef;
typedef struct agl_nodeset_map agl_nodeset_map;
typedef struct agl_node_card agl_node_card;
typedef struct agl_node_rset agl_node_rset;

typedef struct pa_card_hooks pa_card_hooks;
typedef struct pa_port_hooks pa_port_hooks;
typedef struct pa_sink_hooks pa_sink_hooks;
typedef struct pa_source_hooks pa_source_hooks;
typedef struct pa_sink_input_hooks pa_sink_input_hooks;
typedef struct pa_source_output_hooks pa_source_output_hooks;

typedef struct agl_zone agl_zone;
typedef struct agl_node agl_node;
typedef struct agl_rtgroup agl_rtgroup;
typedef struct agl_connection agl_connection;

typedef struct {
    char *profile;
    uint32_t sink;
    uint32_t source;
} pa_agl_state;

#ifdef ENABLE_VAPM
typedef int32_t (*vapm_fe_init_t) (void);
typedef int32_t (*vapm_fe_deinit_t) (void);
typedef vapm_handle_t (*vapm_register_t) (vapm_vm_t, vapm_callback_t);
typedef int32_t (*vapm_deregister_t) (vapm_handle_t);
typedef int32_t (*vapm_request_t) (vapm_handle_t);
typedef int32_t (*vapm_release_t) (vapm_handle_t);
typedef void (*agl_vapm_effect) (struct userdata *, agl_node *, pa_sink_input *, bool);

struct agl_vapm {
	void               *hdl;                /* dynamic link handle */
	vapm_fe_init_t     init;                /* frontend server init */
	vapm_fe_deinit_t   deinit;              /* frontend server deinit */
	vapm_register_t    register_client;     /* client register */
	vapm_deregister_t  deregister_client;   /* client deregister */
	vapm_request_t     request_permission;  /* client request permission */
	vapm_release_t     release_permission;  /* client release permission */
	vapm_callback_t    cb;                  /* frontend callback */
	vapm_handle_t      handle;              /* client handle */
	pthread_mutex_t    lock;                /* mutex */
	int32_t            reg_count;           /* register counter */
	int32_t            act_count;           /* active counter */
};
typedef struct agl_vapm agl_vapm;

struct agl_vapm_node {
	struct agl_vapm_node *prev;
	struct agl_vapm_node *next;
	struct userdata *u;
	agl_node *node;
};
typedef struct agl_vapm_node agl_vapm_node;
#endif

struct userdata {
	pa_core       *core;
	pa_module     *module;
	char          *nsnam;
	agl_zoneset    *zoneset;
	agl_nodeset    *nodeset;
	agl_audiomgr   *audiomgr;
	agl_routerif   *routerif;
	agl_router     *router;
	agl_discover   *discover;
	agl_tracker    *tracker;
	pa_agl_state state;
	snd_use_case_mgr_t *ucm_mgr;
#ifdef ENABLE_VAPM
	agl_vapm       *vapm;
#endif
};

 /* application classes */
typedef enum agl_node_type {
	agl_node_type_unknown = 0,

	agl_application_class_begin,
	agl_radio = agl_application_class_begin,
	agl_player,
	agl_recorder,
	agl_navigator,
	agl_game,
	agl_browser,
	agl_camera,
	agl_phone,                  /**< telephony voice */
	agl_alert,                  /**< ringtone, alarm */
	agl_event,                  /**< notifications */
	agl_system,                 /**< always audible system notifications, events */
	agl_hfp_wb_downlink,
	agl_hfp_wb_uplink,
	agl_hfp_downlink,
	agl_hfp_uplink,
	agl_sdars_tuner_local_amp,
	agl_voice_ecns,
	agl_icc_call,               /**< icc_call */
	agl_application_class_end,
} agl_node_type;

typedef enum  agl_behavior_type {
	agl_behavior_type_unknown = -1,

	agl_behavior_type_begin,
	agl_behavior_type_reject = agl_behavior_type_begin,
	agl_behavior_type_switchoff,
	agl_behavior_type_swithcork,
	agl_behavior_type_switchnull,
	agl_behavior_type_conmute,
	agl_behavior_type_conduck,
	agl_behavior_type_conmix,
	agl_behavior_type_end,
} agl_behavior_type;

typedef enum  agl_ucm_type {
       agl_ucm_type_unknown =-1,

       agl_ucm_type_begin,
       agl_ucm_hifi = agl_ucm_type_begin,
       agl_ucm_navigation,
       agl_ucm_record,
       agl_ucm_hfp,
       agl_ucm_hfp_wb,
       agl_ucm_sdars_tuner_local_amp,
       agl_ucm_voice_ecns,
       agl_ucm_icc_call,
       agl_ucm_type_end,
} agl_ucm_type;
#define AGL_UCM_MAX (agl_ucm_type_end)

typedef enum agl_direction {
	agl_direction_unknown = 0,
	agl_input,
	agl_output
} agl_direction;

typedef enum agl_implement {
	agl_implementation_unknown = 0,
	agl_device,
	agl_stream
} agl_implement;

typedef enum agl_location {
	agl_location_unknown = 0,
	agl_internal,
	agl_external
} agl_location;

typedef enum agl_privacy {
	agl_privacy_unknown = 0,
	agl_public,
	agl_private
} agl_privacy;

#endif
