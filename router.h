/*
 * module-agl-audio -- PulseAudio module for providing audio routing support
 * (forked from "module-murphy-ivi" - https://github.com/otcshare )
 * Copyright (c) 2012, Intel Corporation.
 * Copyright (c) 2016, IoT.bzh
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St - Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 */
#ifndef paaglrouter
#define paaglrouter

#include "userdata.h"
#include "list.h"
#include "node.h"

#define AGL_ZONE_MAX 8	/* max 8 zones, demo is using 5 */ /* DEFINED IN MURPHY */
#define MAX_STREAMLIST 4	/* print route for max 4 stream list */

typedef bool (*agl_rtgroup_accept_t)(struct userdata *, agl_rtgroup *, agl_node *);
typedef int (*agl_rtgroup_effect_t)(struct userdata *, agl_rtgroup *, agl_node *, bool new);

typedef struct agl_accept {
	const char *name;
	uint32_t    index;
}agl_accept;

typedef struct agl_acceptset {
	pa_idxset *accepts;
}agl_acceptset;

struct agl_rtgroup {
	char *name;          /**< name of the rtgroup */
	agl_dlist entries;   /**< listhead of ordered rtentries */
	agl_node *node;      /**< final node */
	agl_acceptset *acceptset; /* accepted function list */
	agl_rtgroup_effect_t effect; /**< function pointer, custom action such as volume up, down */
};

typedef struct {
	pa_hashmap *input;
	pa_hashmap *output;
} agl_rtgroup_hash;

typedef struct {
	agl_rtgroup **input[AGL_ZONE_MAX];
	agl_rtgroup **output[AGL_ZONE_MAX];
} agl_rtgroup_classmap;

typedef struct {
	pa_hashmap *casemap[AGL_UCM_MAX];
	pa_hashmap *behaviormap;
}agl_behavior_hash;

struct agl_router {
	agl_rtgroup_hash rtgroups;
	agl_behavior_hash behaviormatrix;
	size_t maplen;		      /**< length of the class */
	agl_rtgroup_classmap classmap; /**< map device node types to rtgroups */
	int *priormap;                /**< stream node priorities */
	agl_dlist nodlist;            /**< priorized list of the stream nodes
                                        (entry in node: rtprilist) */
	agl_dlist connlist;           /**< listhead of the connections */
};

struct agl_connection {
	agl_dlist link;    /**< list of connections */
	agl_dlist strlist; /**< listhead of same client stream connections */
	bool blocked;      /**< true if this conflicts with another route */
	uint16_t amid;     /**< audio manager connection id */
	uint32_t from;     /**< source node index */
	uint32_t to;       /**< destination node index */
	uint32_t stream;   /**< index of the sink-input to be routed */
	agl_node *node;
	agl_behavior_type behavior;
	agl_rtgroup_effect_t effect; /**< function pointer, custom action such as volume up, down */
	agl_rtgroup_effect_t suppress; /**< function pointer, custom action such as switch null, cork */
	agl_connection *guest;  /**< guest is the connect that suppressed by current connect */
	agl_connection *host;   /**< host is the connect that suppress current connect */
	uint32_t idx;
	const char *usecase;
	const char *effect_name;
	const char *suppress_name;
};

agl_router *agl_router_init (struct userdata *);
void agl_router_done (struct userdata *);

bool agl_router_default_accept (struct userdata *, agl_rtgroup *, agl_node *);
bool agl_router_phone_accept (struct userdata *, agl_rtgroup *, agl_node *);
int agl_router_default_effect (struct userdata *, agl_rtgroup *, agl_node *, bool);
int agl_router_phone_effect (struct userdata *, agl_rtgroup *, agl_node *, bool);
int agl_router_duck_effect (struct userdata *, agl_rtgroup *, agl_node *, bool);
int agl_router_mute_effect (struct userdata *, agl_rtgroup *, agl_node *, bool);
int agl_router_cork_effect (struct userdata *, agl_rtgroup *, agl_node *, bool);
int agl_router_switch_effect (struct userdata *, agl_rtgroup *, agl_node *, bool);
int agl_router_switchoff_effect (struct userdata *, agl_rtgroup *, agl_node *, bool);
int agl_router_reject_effect (struct userdata *, agl_rtgroup *, agl_node *, bool);
int agl_router_mix_effect (struct userdata *, agl_rtgroup *, agl_node *, bool);
agl_rtgroup_effect_t agl_router_get_effect_fct (const char *);

agl_acceptset *agl_acceptset_init (void);
void agl_acceptset_done (agl_rtgroup *);
int agl_acceptset_add_effect (agl_acceptset *, const char *);
bool agl_find_accept_fct(agl_acceptset *, const char *);

agl_behavior_type agl_behavior_type_from_str (const char *);
void agl_router_add_behavior_matrix(struct userdata *, const char *, const char *, const char *);
agl_behavior_type agl_router_get_behavior_type(struct userdata *, const char *, const char *);

agl_rtgroup *agl_router_create_rtgroup (struct userdata *, agl_direction, const char *, const char *, agl_acceptset *, agl_rtgroup_effect_t);
void agl_router_destroy_rtgroup (struct userdata *, agl_direction, const char *);
bool agl_router_assign_class_to_rtgroup (struct userdata *, agl_node_type, uint32_t, agl_direction, const char *);
agl_rtgroup * agl_router_get_rtgroup_from_class (struct userdata *u, agl_node_type class, uint32_t zone, agl_direction type);
void agl_router_assign_class_priority (struct userdata *, agl_node_type, int);
int agl_router_get_node_priority (struct userdata *, agl_node *);
bool agl_router_apply_node_priority_effect (struct userdata *, agl_node *, bool);

void agl_router_register_node (struct userdata *, agl_node *);
void agl_router_unregister_node (struct userdata *, agl_node *);
agl_node *agl_router_make_prerouting (struct userdata *, agl_node *);
void agl_router_make_routing (struct userdata *);

void implement_default_route (struct userdata *, agl_node *, agl_node *, uint32_t);
agl_node *find_default_route (struct userdata *, agl_node *, uint32_t);
void implement_route (struct userdata *u, agl_node *, agl_node *, uint32_t);
void remove_routes (struct userdata *, agl_node *, agl_node*, uint32_t);

#endif
