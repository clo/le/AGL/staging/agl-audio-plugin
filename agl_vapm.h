/***
agl_vapm.h -- Virtualized audio policy manager is a module that loaded
by agl-audio-plugin. It used to register, deregister, request and
release permission to PVM VAPM backend server for virtualized audio
policy controls.

Copyright (c) 2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/

#ifndef paaglvapm
#define paaglvapm

#include "userdata.h"
#include "node.h"

agl_vapm *agl_vapm_init (struct userdata *);
void agl_vapm_done (struct userdata *);
int32_t agl_vapm_create_fe_service (struct userdata *);

int32_t agl_vapm_register_client (struct userdata *, agl_node *);
int32_t agl_vapm_deregister_client (struct userdata *, agl_node *);
int32_t agl_vapm_request_permission (struct userdata *, agl_node *);
int32_t agl_vapm_release_permission (struct userdata *, agl_node *);

int32_t agl_vapm_callback (vapm_cb_param *);
#endif
