/*
 * module-agl-audio -- PulseAudio module for providing audio routing support
 * (forked from "module-murphy-ivi" - https://github.com/otcshare )
 * Copyright (c) 2012, Intel Corporation.
 * Copyright (c) 2016, IoT.bzh
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU Lesser General Public License,
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St - Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 */
#include "config.h"
#include "zone.h"

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <json-c/json.h>

#include <pulsecore/core-util.h>
#include <pulsecore/pulsecore-config.h>

bool use_default_configuration (struct userdata *);

const char *agl_config_file_get_path (const char *dir, const char *file, char *buf, size_t len)
{
	pa_assert (file);
	pa_assert (buf);
	pa_assert (len > 0);

	snprintf (buf, len, "%s/%s", dir, file);

	return buf;
}

bool agl_config_parse_file (struct userdata *u, const char *path)
{
	bool success;

	pa_assert (u);

	if (!path)
		return false;
	else {
		pa_log_info ("parsing configuration file '%s'", path);
		success = agl_config_dofile (u, path);
	}

	if (!success) {
		pa_log_info ("applying builtin default configuration");
		success = use_default_configuration (u);
	}

	return success;
}

bool agl_config_dofile (struct userdata *u, const char *path)
{
	int filefd;
	struct stat filestat;
	void *filemap;
	struct json_object *fjson, *root, *sct, *elt, *selt, *sct2, *sct0, *tmp;
	const char *val;
	int len, i, len2, j;

	pa_assert (u);
	pa_assert (path);

	filefd = open (path, O_RDONLY);
	if (filefd == -1) {
		pa_log_info ("could not find configuration file '%s'", path);
		return false;
	}
	fstat (filefd, &filestat);

	filemap = mmap (NULL, filestat.st_size, PROT_READ, MAP_PRIVATE, filefd, 0);
	if (filemap == MAP_FAILED) {
		pa_log_info ("could not map configuration file in memory");
		return false;
	}

	 /* is the file a JSON file, and if it is, does it have a "config" root ? */
	fjson = json_tokener_parse (filemap);
	json_object_object_get_ex (fjson, "config", &root);
	if (!fjson || !root) {
		pa_log_info ("could not parse JSON configuration file");
		return false;
	}

	 /* [zones] section */
	json_object_object_get_ex (root, "zones", &sct);
	if (!sct) return false;
	len = json_object_array_length (sct);
	for (i = 0; i < len; i++) {
		elt = json_object_array_get_idx (sct, i);
		val = json_object_get_string (elt);
		agl_zoneset_add_zone (u, val, (uint32_t)i);
	}

	 /* [rtgroups] section */
	json_object_object_get_ex (root, "rtgroups", &sct);
	if (!sct) return false;
	len = json_object_array_length (sct);
	for (i = 0; i < len; i++) {
		const char *name, *type, *card, *accept_fct, *effect_fct;
		elt = json_object_array_get_idx (sct, i);
		json_object_object_get_ex (elt, "name", &tmp);
		if (!tmp) return false;
		name = json_object_get_string (tmp);
		json_object_object_get_ex (elt, "type", &tmp);
		if (!tmp) return false;
		type = json_object_get_string (tmp);
		json_object_object_get_ex (elt, "card", &tmp);
		if (!tmp) return false;
		card = json_object_get_string (tmp);
		json_object_object_get_ex (elt, "accept_fct", &sct2);
		if (!sct2)
			pa_log_info("Doesn't have accept function list.");
		agl_acceptset *acceptset = agl_acceptset_init ();
		agl_accept *accept_list;
		if(sct2) {
			len2 = json_object_array_length (sct2);
			for (j = 0; j < len2; j++) {
				selt = json_object_array_get_idx (sct2, j);
				accept_fct = json_object_get_string(selt);
				agl_acceptset_add_effect (acceptset, accept_fct);
            }
		}
		json_object_object_get_ex (elt, "effect_fct", &tmp);
		if (tmp)
			effect_fct = json_object_get_string (tmp);
		else
			effect_fct = NULL;
		agl_router_create_rtgroup (u, pa_streq(type, "OUTPUT") ? agl_output : agl_input,
					      name, card,
					      acceptset,
					      agl_router_get_effect_fct (effect_fct));
	}

	 /* [classmap] section */
	json_object_object_get_ex (root, "classmap", &sct);
	if (!sct) return false;
	len = json_object_array_length (sct);
	for (i = 0; i < len; i++) {
		const char *class, *type, *rtgroup;
		int zone;
		elt = json_object_array_get_idx (sct, i);
		json_object_object_get_ex (elt, "class", &tmp);
		if (!tmp) return false;
		class = json_object_get_string (tmp);
		json_object_object_get_ex (elt, "type", &tmp);
		if (!tmp) return false;
		type = json_object_get_string (tmp);
		json_object_object_get_ex (elt, "zone", &tmp);
		if (!tmp) return false;
		zone = json_object_get_int (tmp);
		json_object_object_get_ex (elt, "rtgroup", &tmp);
		if (!tmp) return false;
		rtgroup = json_object_get_string (tmp);
		agl_router_assign_class_to_rtgroup (u, agl_node_type_from_str (class),
						       zone,
						       pa_streq(type, "OUTPUT") ? agl_output : agl_input,
						       rtgroup);
	}

	 /* [typemap] section */
	json_object_object_get_ex (root, "typemap", &sct);
	if (!sct) return false;
	len = json_object_array_length (sct);
	for (i = 0; i < len; i++) {
		const char *id, *type;
		elt = json_object_array_get_idx (sct, i);
		json_object_object_get_ex (elt, "id", &tmp);
		if (!tmp) return false;
		id = json_object_get_string (tmp);
		json_object_object_get_ex (elt, "type", &tmp);
		if (!tmp) return false;
		type = json_object_get_string (tmp);
		agl_nodeset_add_role (u, id, agl_node_type_from_str (type), NULL);
	}

	 /* [priormap] section */
	json_object_object_get_ex (root, "priormap", &sct);
	if (!sct) return false;
	len = json_object_array_length (sct);
	for (i = 0; i < len; i++) {
		const char *class;
		int priority;
		elt = json_object_array_get_idx (sct, i);
		json_object_object_get_ex (elt, "class", &tmp);
		if (!tmp) return false;
		class = json_object_get_string (tmp);
		json_object_object_get_ex (elt, "priority", &tmp);
		if (!tmp) return false;
		priority = json_object_get_int (tmp);
		agl_router_assign_class_priority (u, agl_node_type_from_str (class), priority);
	}

		/* [usecasemap] section */
	json_object_object_get_ex (root, "usecasemap", &sct);
	if (!sct) return false;
	len = json_object_array_length (sct);
	for (i = 0; i < len; i++) {
		const char *media_role;
		const char *ucm_verb;
		elt = json_object_array_get_idx (sct, i);
		json_object_object_get_ex (elt, "media_role", &tmp);
		if (!tmp) return false;
		media_role = json_object_get_string (tmp);
		json_object_object_get_ex (elt, "ucm_verb", &tmp);
		if (!tmp) return false;
		ucm_verb = json_object_get_string (tmp);
		agl_snd_card_utils_add_usecasemap (media_role, ucm_verb);
	}

	 /* [behaviorMatrix] section */
	json_object_object_get_ex(root, "behaviorMatrix", &sct);
	if (!sct) return false;
	len = json_object_array_length (sct);
	elt = json_object_array_get_idx (sct, 0);
	json_object_object_get_ex(elt, "initCase", &sct0);
	if (!sct0) return false;
	for (i = 1; i < len; i++) {
		const char *initcase;
		const char *triggercase;
		const char *behavior;
		agl_router *router = u->router;
		elt = json_object_array_get_idx (sct, i);
		json_object_object_get_ex (elt, "triggerCase", &tmp);
		if (!tmp) return false;
		triggercase = json_object_get_string (tmp);
		json_object_object_get_ex(elt, "behavior", &sct2);
		if (!sct2) return false;
		len2 = json_object_array_length (sct2);
		for (j = 0; j < len2; j++) {
			selt = json_object_array_get_idx (sct2, j);
			behavior = json_object_get_string (selt);
			initcase = json_object_get_string (json_object_array_get_idx (sct0, j));
			agl_router_add_behavior_matrix (u, initcase, triggercase, behavior);
		}
	}

	 /* [audio_sinks] section */
	json_object_object_get_ex (root, "audio_sinks", &sct);
	if (!sct) return false;
	len = json_object_array_length (sct);
	for (i = 0; i < len; i++) {
		agl_audio_sink_prop sink_prop;
		memset (&sink_prop, 0 , sizeof(agl_audio_sink_prop));
		elt = json_object_array_get_idx (sct, i);
		json_object_object_get_ex (elt, "name", &tmp);
		if (!tmp) return false;
		sink_prop.name = json_object_get_string (tmp);
		json_object_object_get_ex (elt, "device", &tmp);
		if (!tmp) return false;
		sink_prop.device = json_object_get_string (tmp);
		json_object_object_get_ex (elt, "ucm_verb", &tmp);
		if (!tmp) return false;
		sink_prop.ucm_verb= pa_xstrdup(json_object_get_string (tmp));
		json_object_object_get_ex (elt, "num_channels", &tmp);
		if (!tmp) return false;
		sink_prop.num_channels= json_object_get_int (tmp);
		json_object_object_get_ex (elt, "sampling_rate", &tmp);
		if (!tmp) return false;
		sink_prop.sampling_rate= json_object_get_int (tmp);
		json_object_object_get_ex (elt, "fragment_size", &tmp);
		if (!tmp) return false;
		sink_prop.fragment_size= json_object_get_int (tmp);
		json_object_object_get_ex (elt, "fragment_count", &tmp);
		if (!tmp) return false;
		sink_prop.fragment_count= json_object_get_int (tmp);
		json_object_object_get_ex (elt, "format", &tmp);
		if (!tmp) return false;
		sink_prop.format= json_object_get_string (tmp);
		json_object_object_get_ex (elt, "mmap", &tmp);
		if (!tmp) return false;
		sink_prop.mmap= json_object_get_int (tmp);
		agl_snd_card_utils_add_sink_prop (&sink_prop);
	}

	 /* [audio_sources] section */
	json_object_object_get_ex (root, "audio_sources", &sct);
	if (!sct) return false;
	len = json_object_array_length (sct);
	for (i = 0; i < len; i++) {
		agl_audio_source_prop source_prop;
		memset (&source_prop, 0 , sizeof(agl_audio_source_prop));
		elt = json_object_array_get_idx (sct, i);
		json_object_object_get_ex (elt, "name", &tmp);
		if (!tmp) return false;
		source_prop.name = json_object_get_string (tmp);
		json_object_object_get_ex (elt, "device", &tmp);
		if (!tmp) return false;
		source_prop.device = json_object_get_string (tmp);
		json_object_object_get_ex (elt, "ucm_verb", &tmp);
		if (!tmp) return false;
		source_prop.ucm_verb= pa_xstrdup(json_object_get_string (tmp));
		json_object_object_get_ex (elt, "num_channels", &tmp);
		if (!tmp) return false;
		source_prop.num_channels= json_object_get_int (tmp);
		json_object_object_get_ex (elt, "sampling_rate", &tmp);
		if (!tmp) return false;
		source_prop.sampling_rate= json_object_get_int (tmp);
		json_object_object_get_ex (elt, "fragment_size", &tmp);
		if (!tmp) return false;
		source_prop.fragment_size= json_object_get_int (tmp);
		json_object_object_get_ex (elt, "fragment_count", &tmp);
		if (!tmp) return false;
		source_prop.fragment_count= json_object_get_int (tmp);
		json_object_object_get_ex (elt, "format", &tmp);
		if (!tmp) return false;
		source_prop.format= json_object_get_string (tmp);
		json_object_object_get_ex (elt, "mmap", &tmp);
		if (!tmp) return false;
		source_prop.mmap= json_object_get_int (tmp);
		agl_snd_card_utils_add_source_prop (&source_prop);
	}

	json_object_object_del (fjson, "");
	munmap (filemap, filestat.st_size);
	close (filefd);

	return true;
}


 /* DEFAULT CONFIGURATION PART */

static zone_def zones[] = {
	{ "driver" },
	{ "passenger1" },
	{ "passenger2" },
	{ "passenger3" },
	{ "passenger4" },
	{ NULL }
};

static rtgroup_def rtgroups[] = {
	{ agl_input,
	  "default",
	  "alsa_output.MultiMedia1",
	  NULL,
	  NULL
	},

	{ agl_input,
	  "Player",
	  "alsa_output.MultiMedia1",
	  NULL,
	  NULL
	},

	{ agl_output,
	  "Recorder",
	  "alsa_input.MultiMedia1",
	  NULL,
	  NULL
	},

	{ agl_input,
	  "Navigator",
	  "alsa_output.MultiMedia2",
	  NULL,
	  NULL
	},

	{ agl_input,
	  "HfpDownlink",
	  "alsa_output.HfpDownlink",
	  NULL,
	  NULL
	},

	{ agl_output,
	  "HfpDownlink",
	  "alsa_input.HfpDownlink",
	  NULL,
	  NULL
	},

	{ agl_input,
	  "HfpUplink",
	  "alsa_output.HfpUplink",
	  NULL,
	  NULL
	},

	{ agl_output,
	  "HfpUplink",
	  "alsa_input.HfpUplink",
	  NULL,
	  NULL
	},

	{ agl_input,
	  "SDARS_tuner_local_amp",
	  "alsa_output.sdars_tuner_hostless",
	  NULL,
	  NULL
	},

	{ agl_output,
	  "SDARS_tuner_local_amp",
	  "alsa_input.sdars_tuner_hostless",
	  NULL,
	  NULL
	},

	{ agl_input,
	  "IccCall",
	  "alsa_output.MultiMedia9",
	  NULL,
	  NULL
	},

	{ agl_output,
	  "IccCall",
	  "alsa_input.MultiMedia9",
	  NULL,
	  NULL
	},

	{ 0, NULL, NULL, NULL, NULL }
};

static classmap_def classmap[] = {
	{ agl_player,	0, agl_input, "Player" },
	{ agl_recorder,	0, agl_output, "Recorder" },
	{ agl_navigator,0, agl_input, "Navigator" },
	{ agl_hfp_wb_downlink,	0, agl_input, "HfpDownlink" },
	{ agl_hfp_wb_downlink, 0, agl_output, "HfpDownlink" },
	{ agl_hfp_wb_uplink, 0, agl_input, "HfpUplink" },
	{ agl_hfp_wb_uplink, 0, agl_output, "HfpUplink" },
	{ agl_hfp_downlink, 0, agl_input, "HfpDownlink" },
	{ agl_hfp_downlink, 0, agl_output, "HfpDownlink" },
	{ agl_hfp_uplink, 0, agl_input, "HfpUplink" },
	{ agl_hfp_uplink, 0, agl_output, "HfpUplink" },
	{ agl_sdars_tuner_local_amp, 0, agl_input, "SDARS_tuner_local_amp" },
	{ agl_sdars_tuner_local_amp, 0, agl_output, "SDARS_tuner_local_amp" },
	{ agl_icc_call, 0, agl_input, "IccCall" },
	{ agl_icc_call, 0, agl_output, "IccCall" },
	{agl_voice_ecns, 0, agl_output, "Recorder" },
	{ agl_node_type_unknown, 0, agl_direction_unknown, NULL }
};

static typemap_def typemap[] = {
	{ "music", agl_player },
    { "record", agl_recorder  },
	{ "navi", agl_navigator },
	{ "hfp_wb_downlink", agl_hfp_wb_downlink },
	{ "hfp_wb_uplink", agl_hfp_wb_uplink },
	{ "hfp_downlink", agl_hfp_downlink },
	{ "hfp_uplink", agl_hfp_uplink },
	{ "sdars_tuner_local_amp", agl_sdars_tuner_local_amp },
	{ "icc_call", agl_icc_call },
	{"voice_ecns", agl_voice_ecns  },
	{ NULL, agl_node_type_unknown }
};

static prior_def priormap[] = {
	{ agl_navigator, 3 },
	{ agl_player,	 1 },
    { agl_recorder, 2 },
	{ agl_hfp_wb_downlink, 4 },
	{ agl_hfp_wb_uplink, 4 },
	{ agl_hfp_downlink, 4 },
	{ agl_hfp_uplink, 4 },
	{ agl_sdars_tuner_local_amp, 1 },
	{ agl_icc_call, 2 },
	{ agl_voice_ecns, 2 },
	{ agl_node_type_unknown, 0 }
};

static usecase_def usecasemap[] = {
	{"record", "Record"},
	{"music", "HiFi"},
	{"navi", "Navigation"},
	{"hfp_wb_downlink", "Hfp_wb"},
	{"hfp_wb_uplink", "Hfp_wb"},
	{"hfp_downlink", "Hfp"},
	{"hfp_uplink", "Hfp"},
	{"sdars_tuner_local_amp", "SDARS Tuner Local AMP"},
	{"icc_call", "Icc_call"},
	{"voice_ecns", "Voice_ECNS"},
	{NULL, NULL}
};

/* {"initcase", "triggercase", "behavior"} */
static behavior_def behaviorMatrix[] = {
	{"Record",                "Record", "SwitchOff"},
	{"HiFi",                  "Record", "SwitchCork"},
	{"Navigation",            "Record", "ConMix"},
	{"Hfp_wb",                "Record", "Reject"},
	{"Hfp",                   "Record", "Reject"},
	{"SDARS Tuner Local AMP", "Record", "SwitchNull"},
	{"Voice_ECNS",            "Record", "SwitchOff"},
	{"Icc_call",              "Record", "SwitchOff"},
	{"Record",                "HiFi", "Reject"},
	{"HiFi",                  "HiFi", "SwitchCork"},
	{"Navigation",            "HiFi", "ConDuck"},
	{"Hfp_wb",                "HiFi", "ConDuck"},
	{"Hfp",                   "HiFi", "ConDuck"},
	{"SDARS Tuner Local AMP", "HiFi", "SwitchNull"},
	{"Voice_ECNS",            "HiFi", "ConMix"},
	{"Icc_call",              "HiFi", "ConMix"},
	{"Record",                "Navigation", "ConMix"},
	{"HiFi",                  "Navigation", "ConDuck"},
	{"Navigation",            "Navigation", "SwitchNull"},
	{"Hfp_wb",                "Navigation", "ConMix"},
	{"Hfp",                   "Navigation", "ConMix"},
	{"SDARS Tuner Local AMP", "Navigation", "ConDuck"},
	{"Voice_ECNS",            "Navigation", "ConMix"},
	{"Icc_call",              "Navigation", "ConMix"},
	{"Record",                "Hfp_wb", "SwitchNull"},
	{"HiFi",                  "Hfp_wb", "SwitchCork"},
	{"Navigation",            "Hfp_wb", "ConMix"},
	{"Hfp_wb",                "Hfp_wb", "SwitchNull"},
	{"Hfp",                   "Hfp_wb", "SwitchNull"},
	{"SDARS Tuner Local AMP", "Hfp_wb", "SwitchNull"},
	{"Voice_ECNS",            "Hfp_wb", "SwitchOff"},
	{"Icc_call",              "Hfp_wb", "SwitchOff"},
	{"Record",                "Hfp", "SwitchNull"},
	{"HiFi",                  "Hfp", "SwitchCork"},
	{"Navigation",            "Hfp", "ConMix"},
	{"Hfp_wb",                "Hfp", "SwitchNull"},
	{"Hfp",                   "Hfp", "SwitchNull"},
	{"SDARS Tuner Local AMP", "Hfp", "SwitchNull"},
	{"Voice_ECNS",            "Hfp", "SwitchOff"},
	{"Icc_call",              "Hfp", "SwitchOff"},
	{"Record",                "SDARS Tuner Local AMP", "Reject"},
	{"HiFi",                  "SDARS Tuner Local AMP", "SwitchCork"},
	{"Navigation",            "SDARS Tuner Local AMP", "ConMix"},
	{"Hfp_wb",                "SDARS Tuner Local AMP", "ConMix"},
	{"Hfp",                   "SDARS Tuner Local AMP", "ConMix"},
	{"SDARS Tuner Local AMP", "SDARS Tuner Local AMP", "SwitchNull"},
	{"Voice_ECNS",            "SDARS Tuner Local AMP", "Reject"},
	{"Icc_call",              "SDARS Tuner Local AMP", "Reject"},
	{"Record",                "Icc_call", "SwitchOff"},
	{"HiFi",                  "Icc_call", "ConMix"},
	{"Navigation",            "Icc_call", "ConMix"},
	{"Hfp_wb",                "Icc_call", "Reject"},
	{"Hfp",                   "Icc_call", "Reject"},
	{"SDARS Tuner Local AMP", "Icc_call", "SwitchNull"},
	{"Voice_ECNS",            "Voice_ECNS", "SwitchOff"},
	{"Icc_call",              "Icc_call", "SwitchOff"},
	{NULL, NULL, NULL}
};

/* {"name", "device", "ucm_verb", "num_channels", "sampling_rate", "fragment_size", "fragment_count", "format", "mmap"} */
static agl_audio_sink_prop audio_sinks[] = {
	{"alsa_output.MultiMedia1", "hw:0,0", "HiFi", 2, 48000, 4800, 4, "s16le", 1},
	{"alsa_output.MultiMedia2", "hw:0,1", "Navigation", 2, 48000, 4800, 4, "s16le", 1},
	{"alsa_output.HfpDownlink", "hw:0,52", "Hfp_wb", 1, 16000, 240, 2, "s16le", 0},
	{"alsa_output.HfpUplink", "hw:0,24", "Hfp_wb", 1, 16000, 240, 2, "s16le", 0},
	{"alsa_output.sdars_tuner_hostless", "hw:0,50", "SDARS Tuner Local AMP", 2, 48000, 240, 2, "s16le", 0},
	{"alsa_output.MultiMedia9", "hw:0,32", "Icc_call", 1, 16000, 240, 2, "s16le" ,0},
	{NULL, NULL, NULL, 0, 0, 0, 0, NULL, 0}
};

static agl_audio_source_prop audio_sources[] = {
	{"alsa_input.MultiMedia1", "hw:0,0", "Record", 2, 48000, 4800, 4, "s16le", 1},
	{"alsa_input.HfpDownlink", "hw:0,23", "Hfp_wb", 1, 16000, 240, 2, "s16le", 0},
	{"alsa_input.HfpUplink", "hw:0,24", "Hfp_wb", 1, 16000, 240, 2, "s16le", 0},
	{"alsa_input.sdars_tuner_hostless", "hw:0,59", "SDARS Tuner Local AMP", 2, 48000, 240, 2, "s16le", 0},
	{"alsa_input.MultiMedia9", "hw:0,32", "Icc_call", 1, 16000, 240, 2, "s16le" , 0},
	{NULL, NULL, NULL, 0, 0, 0, 0, NULL, 0}
};

bool use_default_configuration (struct userdata *u)
{
	zone_def *z;
	rtgroup_def *r;
	classmap_def *c;
	typemap_def *t;
	prior_def *p;
	usecase_def *uc;
	behavior_def *bm;
	agl_audio_sink_prop sinkp;
	agl_audio_source_prop sourcep;
	int i;

	pa_assert (u);

	for (z = zones; z->name; z++)
		agl_zoneset_add_zone (u, z->name, (uint32_t)(z - zones));

	for (r = rtgroups; r->name; r++)
		agl_router_create_rtgroup (u, r->type, r->name, r->node_desc,
					      r->accept, r->effect);

	for (c = classmap; c->rtgroup; c++)
		agl_router_assign_class_to_rtgroup (u, c->class, c->zone,
						       c->type, c->rtgroup);

	for (t = typemap; t->id; t++) 
		agl_nodeset_add_role (u, t->id, t->type, NULL);

	for (p = priormap; p->class; p++)
		agl_router_assign_class_priority (u, p->class, p->priority);

	for (uc = usecasemap; uc->media_role; uc++)
		agl_snd_card_utils_add_usecasemap (uc->media_role, uc->ucm_verb);

	for (bm = behaviorMatrix; bm->initcase; bm++)
		agl_router_add_behavior_matrix (u, bm->initcase, bm->triggercase, bm->behavior);

	for (i = 0; ; i++)
	{
		sinkp = audio_sinks[i];
		if (sinkp.name == NULL)
			break;
		agl_snd_card_utils_add_sink_prop (&sinkp);
	}

	for (i = 0; ; i++)
	{
		sourcep = audio_sources[i];
		if (sourcep.name == NULL)
			break;
		agl_snd_card_utils_add_source_prop (&sourcep);
	}

	return true;
}
